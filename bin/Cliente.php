<?php
// set ip and port
$host = "127.0.0.1";
$port = 4096;
// don't timeout!
set_time_limit(0);
// create socket
$socket = socket_create(AF_INET, SOCK_STREAM, 0);
// bind socket to port
$result = socket_bind($socket, $host, $port);
// start listening for connections
$result = socket_listen($socket, 3);

// accept incoming connections
// spawn another socket to handle communication
$spawn = socket_accept($socket) or die("No se logró aceptar la conexión\n");
// read client input
$input = socket_read($spawn, 1024) or die("No se puede leer la entrada\n");
// clean up input string
$input = trim($input);
echo "Mensaje del cliente : ".$input."<br />";
// reverse client input and send back
$output = strrev($input) ."<br />";
socket_write($spawn, $output, strlen ($output)) or die("No se pudo escribir la salida\n");
// close sockets
socket_close($spawn);
socket_close($socket);
?>
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
require_once 'lib/class.websocket_client.php';
$clients = array();
$testClients = 30;
$testMessages = 500;
for($i = 0; $i < $testClients; $i++)
{
	$clients[$i] = new WebsocketClient;
	$clients[$i]->connect('127.0.0.1', 8000, '/demo', 'foo.lh');
}
usleep(5000);
$payload = json_encode(array(
	'action' => 'echo',
	'data' => 'dos'
));
for($i = 0; $i < $testMessages; $i++)
{
	$clientId = rand(0, $testClients-1);
	$clients[$clientId]->sendData($payload);
	usleep(5000);
}
usleep(5000);