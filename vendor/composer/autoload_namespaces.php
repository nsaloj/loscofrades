<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'cofrades' => array($baseDir . '/app'),
    'Phroute' => array($vendorDir . '/phroute/phroute/src'),
    'Monolog' => array($vendorDir . '/monolog/monolog/src'),
    'Guzzle\\Stream' => array($vendorDir . '/guzzle/stream'),
    'Guzzle\\Parser' => array($vendorDir . '/guzzle/parser'),
    'Guzzle\\Http' => array($vendorDir . '/guzzle/http'),
    'Guzzle\\Common' => array($vendorDir . '/guzzle/common'),
    'Evenement' => array($vendorDir . '/evenement/evenement/src'),
);
