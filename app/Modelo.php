<?php
class Modelo
{
    protected $_db; 

    public function __construct() 
    {
        $config = Config::PSingleton();
        $this->_db = new mysqli($config->get('dbhost'), $config->get('dbuser'), $config->get('dbpass'), $config->get('dbname')); 

        if ( $this->_db->connect_errno ) 
        { 
            echo "Fallo al conectar a MySQL: ". $this->_db->connect_error; 
            return;     
        } 

        $this->_db->set_charset("utf8"); 
    } 
}
?>