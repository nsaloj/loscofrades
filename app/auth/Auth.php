<?php

class Auth
{
    public function __construct() 
    { 
        
    }
    static function loguear($datos)
    {        
        require_once 'app/controllers/LoginController.php';
        $auth = new LoginController();
        if(!self::check())
        {
            $user = $auth->loguear($datos);
            if(!empty($user))
            {
                $_SESSION['userId'] = $user['id'];                
                if(!empty($datos['recordar']))
                {
                    setcookie("userHash", $user['hash'], time()+(60*60*24*365));
                    setcookie("userId", $user['id'], time()+(60*60*24*365));                    
                }
                return true;
            }
            else{
                return false;
            }
        }
        else return true;
    }
    static function user()
    {
        if(self::check())
        {
            return $_SESSION['userId'];
        }
        else return false;
    }
    static function getUser()
    {
        if(self::check())
        {
            require_once 'app/controllers/LoginController.php';
            $auth = new LoginController();
            return $auth->getUsuario(Auth::user());
        }
        else return false;
    }
    static function check()
    {
        require_once 'app/controllers/LoginController.php';
        $auth = new LoginController();
        if(isset($_SESSION['userId']))
        {
            $userId = $_SESSION['userId'];           
            $data = $auth->existeSesion(array("userId"=>$userId,"userHash"=>""));
            return $data;
        }
        else if (isset($_COOKIE["userId"]) && isset($_COOKIE["userHash"])){
            if ($_COOKIE["userId"]!="" || $_COOKIE["userHash"]!=""){
                $userId = $_COOKIE['userId'];
                $userHash = $_COOKIE['userHash'];
                $data = $auth->existeSesion(array("userId"=>$userId,"userHash"=>$userHash));
                if($data === true){
                    $_SESSION['userId'] = $_COOKIE['userId'];  
                }
                return $data;
            }
        }
        else return false;
    }
    static function logout()
    {
        session_destroy();
        if(isset($_COOKIE["userId"])) unset($_COOKIE['userId']);
        if(isset($_COOKIE["userHash"])) unset($_COOKIE['userHash']);
        
        setcookie('userId', "", time()-60);
        setcookie('userHash', "", time()-60);
    }
}
?>