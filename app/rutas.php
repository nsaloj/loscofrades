<?php
$base  = dirname($_SERVER['PHP_SELF']);

// Actualiza cuando se tiene un subdirectorio
if(ltrim($base, '/')){ 

    $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($base));
}


$klein = new \Klein\Klein();

$klein->onHttpError(function ($code, $router) {
    switch ($code) {
        case 404:
            $router->response()->body(
                '404 Página no encontrada'
            );
            break;
        case 405:
            $router->response()->body(
                '404 Página no encontrada'
            );
            break;
        default:
            $router->response()->body(
                'Oh no, un error ha ocurrido, causa: '. $code
            );
    }
});

$klein->respond('GET','/',function($request, $response){
    if(Auth::check())
    {
        require_once ('app/controllers/IndexController.php');
        $index = new IndexController;
        $index::Index();
    }
    else
    {
        $response->redirect('login')->send();
    }
});

$klein->respond('GET','/login',function($request, $response){
    if(Auth::check())
    {
        $response->redirect('.')->send();
    }
    else
    {
        //if(true === false)
        {
            require_once ('app/controllers/LoginController.php');
            $login = new LoginController;
            $login::Index();        
        }
        //else
        {
            
        }
    }        
});
$klein->respond('POST','/acceder',function ($request, $response) use ($klein){
    $respuesta = Auth::loguear(array("user"=>$request->usuario, "password"=>$request->contrasenia, "recordar"=>$request->recordar));
    if($respuesta)
    {
        $response->redirect('.')->send();
    }
    else
    {
        Flash::mensaje(['nombre'=>'login_resp', 'data'=>['tipo'=>'error','mensaje'=>'El usuario y la contraseña no coinciden']]);
        $response->redirect('login')->send();
    }
});

$klein->respond('GET','/registro',function ($request, $response) {
    if(true === true)
    {
        require_once ('app/controllers/LoginController.php');
        $login = new LoginController;
        $login::Registro();        
    } 
    else $response->redirect('.')->send();
});

$klein->respond('POST','/registrar',function ($request, $response) use ($klein){
    if(true === false)
    {
        $respuesta = Auth::loguear(array("user"=>$request->usuario, "password"=>$request->contrasenia, "recordar"=>$request->recordar));
        if($respuesta)
        {
            $response->redirect('.')->send();
        }
        else
        {
            Flash::mensaje(['nombre'=>'login_resp', 'data'=>['tipo'=>'error','mensaje'=>'El usuario y la contraseña no coinciden']]);
            $response->redirect('login')->send();
        }
    }
    else $response->redirect('.')->send();
});

if(Auth::check())
{
$klein->respond('POST','/actualizarPerfil',function ($request, $response) use ($klein){
    if(Auth::check())
    {
        require_once ('app/controllers/UsuarioController.php');
        $usuario = new UsuarioController;
        $usuarioP = json_decode(file_get_contents("php://input"),true);
        $respuesta = $usuario::actualizarDatos($usuarioP);
        /*$respuesta = Auth::cambiar(array("nombre"=>$request->nombre, "apellidos"=>$request->apellidos, "user"=>$request->usuario, "password"=>$request->password, 
        "newpassword"=>$request->newpassword,
        "passwordr"=>$request->passwordr));*/
        return $respuesta;
        /*if($respuesta)
        {
            Flash::mensaje(['nombre'=>'perfil_resp', 'data'=>['tipo'=>'infro','mensaje'=>'Actualizado correctamente']]);
            $response->back()->send();
        }
        else
        {
            Flash::mensaje(['nombre'=>'perfil_resp', 'data'=>['tipo'=>'error','mensaje'=>'Por favor verifique los datos ingresados']]);
            $response->back()->send();
        */
    }
    else $response->redirect('.')->send();
});

$klein->respond('GET','/logout',function ($request, $response){
    Auth::logout();
    $response->redirect('.')->send();
});
$klein->respond('POST','/getIp',function ($request, $response) {
    return getHostByName(php_uname('n')); 
});


$klein->with('/api/v1/mesas', function () use ($klein) {
    $klein->respond('POST','/all',function ($request, $response) {
        require_once ('app/controllers/MesaController.php');
        $mesas = new MesaController;
        return $mesas::getAllMesas();
    });

    $klein->respond(array('POST'), '/get/[:id]', function ($request, $response) {
        require_once ('app/controllers/MesaController.php');
        $mesas = new MesaController;
        return $mesas::getMesa($request->id);
    });
    
    $klein->respond('POST','/crear',function ($request, $response) {
        require_once ('app/controllers/MesaController.php');
        $mesas = new MesaController;
        $mesaP = json_decode(file_get_contents("php://input"),true);       
        return $mesas::crearMesa($mesaP);
    });
    
    $klein->respond(['POST'],'/eliminar/[:id]',function ($request, $response) {
        require_once ('app/controllers/MesaController.php');
        $mesas = new MesaController;
        return $mesas::eliminarMesa($request->id);
    });
    
    $klein->respond(['GET','POST'],'/cambiar/[:pedido]/[:mesa]',function ($request, $response) {
        require_once ('app/controllers/MesaController.php');
        $mesas = new MesaController;
        return $mesas::cambiarMesa($request->pedido,$request->mesa);
    });

});
$klein->with('/api/v1/productos', function () use ($klein) {
    $klein->respond(['POST'],'/all',function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        return $productos::getAllProductos();
    });
    $klein->respond(['POST'],'/total',function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        $productoP = json_decode(file_get_contents("php://input"),true);
        return $productos::getTotal($productoP);
    });
    $klein->respond(['POST'],'/full',function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        $productoP = json_decode(file_get_contents("php://input"),true);
        return $productos::getFullProductos($productoP);
    });
    $klein->respond(array('POST'), '/get/[:id]', function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        return $productos::getProducto($request->id);
    });
    $klein->respond('POST','/crear',function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        $productoP = json_decode(file_get_contents("php://input"),true);
        return $productos::crearProducto($productoP);
    });
    $klein->respond(['POST'],'/eliminar/[:id]',function ($request, $response) {
        require_once ('app/controllers/ProductoController.php');
        $productos = new ProductoController;
        return $productos::eliminar($request->id);
    });
});
$klein->with('/api/v1/categorias', function () use ($klein) {
    $klein->respond(array('POST'), '/get/[:id]', function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getCategoria($request->id);
    });
    $klein->respond(['POST'],'/getCategorias',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getCategorias();
    });
    $klein->respond(['POST'],'/getTop',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getTop();
    });
    $klein->respond(['POST'],'/getHijos',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getHijos();
    });
    $klein->respond(['POST'],'/getArbol',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getArbol();
    });
    $klein->respond(['POST'],'/getproductosCategoria/[:id]',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::getproductosCategoria($request->id);
    });
    $klein->respond('POST','/crear',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        $categoriaP = json_decode(file_get_contents("php://input"),true);
        return $categoria::crearCategoria($categoriaP);
    });
    $klein->respond(['POST'],'/eliminar/[:id]',function ($request, $response) {
        require_once ('app/controllers/CategoriaController.php');
        $categoria = new CategoriaController;
        return $categoria::eliminar($request->id);
    });
});
$klein->with('/api/v1/usuario', function () use ($klein) {
    
    $klein->respond(['POST','GET'],'/get',function ($request, $response) {
        require_once ('app/controllers/UsuarioController.php');
        $usuario = new UsuarioController;
        return $usuario::getUsuario(Auth::user());
    });
    $klein->respond(['POST','GET'],'/secciones',function ($request, $response) {
        require_once ('app/controllers/UsuarioController.php');
        $usuario = new UsuarioController;
        $user = Auth::getUser();
        return $usuario::getSecciones($user["Rol_id"]);
    });
    $klein->respond(['POST'],'/roles',function ($request, $response) {
        require_once ('app/controllers/UsuarioController.php');
        $usuario = new UsuarioController;
        return $usuario::getRoles();
    });
    if(Auth::getUser()["Rol_id"] == 1)
    {
        $klein->respond('POST','/crear',function ($request, $response) {
            require_once ('app/controllers/UsuarioController.php');
            $usuario = new UsuarioController;
            $usuarioP = json_decode(file_get_contents("php://input"),true);
            return $usuario::crearUsuario($usuarioP);
        });
        $klein->respond(['POST'],'/all',function ($request, $response) {
            require_once ('app/controllers/UsuarioController.php');
            $usuario = new UsuarioController;
            return $usuario::getUsuarios();
        });
        $klein->respond(array('POST'), '/get/[:id]', function ($request, $response) {
            require_once ('app/controllers/UsuarioController.php');
            $usuario = new UsuarioController;
            return $usuario::getUsuario($request->id);
        });
        $klein->respond(['POST'],'/eliminar/[:id]',function ($request, $response) {
            require_once ('app/controllers/UsuarioController.php');
            $usuario = new UsuarioController;
            return $usuario::eliminar($request->id);
        });
        
    }
});
$klein->with('/api/v1/informes', function () use ($klein) {
    if(Auth::getUser()["Rol_id"] == 1)
    {
        $klein->respond(['POST'],'/ventas',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            $reportesP = json_decode(file_get_contents("php://input"),true);
            return $reportes::getVentas($reportesP);
        });
        $klein->respond(['POST'],'/total',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            return $reportes::getTotalVendido();
        });
        $klein->respond(['POST'],'/ventasFecha/[:fecha]',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            $reportesP = json_decode(file_get_contents("php://input"),true);
            return $reportes::getVentasFecha($request->fecha);
        });
        $klein->respond(['POST'],'/venta/[:id]',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            $reportesP = json_decode(file_get_contents("php://input"),true);
            return $reportes::getVenta($request->id);
        });
        $klein->respond(['POST'],'/productos',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            $reportesP = json_decode(file_get_contents("php://input"),true);
            return $reportes::getProductos($reportesP);
        });
        $klein->respond(['POST'],'/afluencias',function ($request, $response) {
            require_once ('app/controllers/ReporteController.php');
            $reportes = new ReporteController;
            $reportesP = json_decode(file_get_contents("php://input"),true);
            return $reportes::getAfluencia($reportesP);
        });
    }
});
$klein->with('/api/v1/pedidos', function () use ($klein) {
    $klein->respond(['POST','GET'],'/getListos',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::getListos($request->id);
    });
    $klein->respond(['POST','GET'],'/cambiarEstado/[:id]/[:estado]',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::cambiarEstado($request->id , $request->estado);
    });
    $klein->respond(['POST','GET'],'/getPedidoMesa/[:id]',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::getPedidoMesa($request->id);
    });
    $klein->respond(['POST','GET'],'/getEntregadoMesa/[:id]',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::getEntregadoMesa($request->id);
    });

    $klein->respond('POST','/crear',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        $pedidoP = json_decode(file_get_contents("php://input"),true);
        return $pedido::crearPedido($pedidoP);
    });
    $klein->respond('POST','/pagoParcial',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        $pedidoP = json_decode(file_get_contents("php://input"),true);
        return $pedido::pagoParcial($pedidoP);
    });
    $klein->respond('POST','/pagoTotal',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        $pedidoP = json_decode(file_get_contents("php://input"),true);
        return $pedido::pagoTotal($pedidoP);
    });
    
    $klein->respond(['POST','GET'],'/all',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::getAll();
    });
    $klein->respond(['POST','GET'],'/bebidas',function ($request, $response) {
        require_once ('app/controllers/PedidoController.php');
        $pedido = new PedidoController;
        return $pedido::getBebidas();
    });
});

}
$klein->dispatch();
?>