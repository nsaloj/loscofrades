<?php

class AppController
{
    static function iniciar()
    {
        session_start();
        //Incluimos algunas clases:        
        require 'app/models/Config.php'; //Modelo con patron Singleton de configuracion
        require 'app/config.php'; //archivo de configuraciones
        
        require_once 'app/Modelo.php'; //Modelo de conexion de la bd
        require_once 'app/auth/Flash.php'; //Modelo de conexion de la bd
        require_once 'app/auth/Auth.php'; //Archivo para el control de rutas        
        require_once 'app/rutas.php'; //Archivo para el control de rutas
    }
}
?>