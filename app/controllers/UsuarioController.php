<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Usuario.php");
class UsuarioController extends BaseRest
{
     function actualizarDatos($datos)
     {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->actualizarDatos($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo actualizar el usuario!');
		} else {
			$statusCode = 200;
            if($rawData === true)
            $rawData = array('data' => $rawData);
            else
            $rawData = array('error' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
     function CrearUsuario($datos)
     {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->crearUsuario($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear el usuario!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getUsuario($id)
    {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->getUsuario($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontro el usuario!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,0);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,0);
			echo $response;
		}
    }
    function getUsuarios()
    {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->getUsuarios();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontro el usuario!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		}
    }
    function eliminar($id)
    {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->eliminar($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo eliminar el usuario!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getRoles()
    {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->getRoles();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron roles!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		}
    }
    function getSecciones($id)
    {
        $BaseRest = new BaseRest();
        $usuario = new Usuario();
		$rawData = $usuario->getSecciones($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron secciones');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			var_dump($rawData);
		}
    }
    //Usuario_id, Usuario, Nombre, Apellidos, Rol_id
    public function encodeHtml($responseData, $tipo) {
	
		$htmlResponse = "<table border='1'>";
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                    $htmlResponse .= "<tr><td>". $item['Usuario_id']. "</td><td>". $item['Usuario']. "</td><td>". $item['Nombre']. "</td><td>". $item['Apellidos']. "</td><td>". $item['Rol_id']. "</td></tr>";
            }
        }
        else
        {
            $htmlResponse .= "<tr><td>". $responseData['Usuario_id']. "</td><td>". $responseData['Usuario']. "</td><td>". $responseData['Nombre']. "</td><td>". $responseData['Apellidos']. "</td><td>". $responseData['Rol_id']. "</td></tr>";
        }
		$htmlResponse .= "</table>";
		return $htmlResponse;		
	}
	
	public function encodeJson($responseData, $tipo) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
	public function encodeXml($responseData, $tipo) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                $xml->addChild($item['Usuario_id'], $item['Usuario'],$item['Nombre'],$item['Apellidos'],$item['Rol_id']);
            }
        }
        else
        {
            $xml->addChild($item['Usuario_id'], $item['Usuario'],$item['Nombre'],$item['Apellidos'],$item['Rol_id']);
        }
		return $xml->asXML();
	}
}