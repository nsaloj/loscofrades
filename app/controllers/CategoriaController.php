<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Categoria.php");
class CategoriaController extends BaseRest
{
    function crearCategoria($datos)
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->crearCategoria($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear el producto!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function eliminar($id)
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->eliminar($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo eliminar la categoria!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getCategoria($id)
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getCategoria($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontro la categoria!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		}
    }
    function getCategorias()
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getCategorias();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar la categoria!');
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    function getTop()
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getTop();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar la categoria!');
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    function getHijos()
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getHijos();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar la categoria!');
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    function getArbol()
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getArbol();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar la categoria!');
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    function getproductosCategoria($id)
    {
        $BaseRest = new BaseRest();
        $categoria = new Categoria();
		$rawData = $categoria->getproductosCategoria($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar la categoria!');
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
	public function encodeJson($responseData, $tipo) {
        $jsonResponse = json_encode($responseData);
		return $jsonResponse;
	}
}
?>