<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Productos.php");
class ProductoController extends BaseRest
{
    function crearProducto($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->crearProducto($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear el producto!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		} else if(strpos($requestContentType,'text/html') !== false){
			echo $rawData['data'];
		} else if(strpos($requestContentType,'application/xml') !== false){
            $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
            $xml->addChild($rawData['data']);
			$response = $xml->asXML();
			echo $response;
		}
    }
    function eliminar($id)
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->eliminar($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo eliminar el producto!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getProducto($id)
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->getProducto($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontro el producto!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		}
    }
    function getAllProductos()
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->getAll();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron productos!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getFullProductos($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->getFull($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron productos!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    function getTotal($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Productos();
		$rawData = $productos->getTotal($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron productos!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		}
    }
    
    public function encodeHtml($responseData, $tipo) {
	
		$htmlResponse = "<table border='1'>";
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                    $htmlResponse .= "<tr><td>". $item['Producto_id']. "</td><td>". $item['Nombre']. "</td></tr>";
            }
        }
        else
        {
            $htmlResponse .= "<tr><td>". $responseData['Producto_id']. "</td><td>". $responseData['Nombre']. "</td></tr>";
        }
		$htmlResponse .= "</table>";
		return $htmlResponse;		
	}
	
	public function encodeJson($responseData, $tipo) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
	public function encodeXml($responseData, $tipo) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                $xml->addChild($item['Producto_id'], $item['Nombre']);
            }
        }
        else
        {
            $xml->addChild($responseData['Producto_id'], $responseData['Nombre']);
        }
		return $xml->asXML();
	}
}
?>