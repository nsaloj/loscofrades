<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Reporte.php");
class ReporteController extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    function getVentas($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getVentas($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron ventas!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getTotalVendido()
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getTotalVendido();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron ventas!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getVenta($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getVenta($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron ventas!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getVentasFecha($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getVentasFecha($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron ventas!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getProductos($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getProductos($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron productos!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getAfluencia($datos)
    {
        $BaseRest = new BaseRest();
        $productos = new Reporte();
		$rawData = $productos->getAfluencia($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron productos!');
		} else {
			$statusCode = 200;
            //$rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    
    public function encodeJson($responseData, $tipo) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
}
?>