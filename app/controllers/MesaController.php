<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Mesas.php");
class MesaController extends BaseRest
{
    function crearMesa($datos)
    {
        $BaseRest = new BaseRest();
        $mesas = new Mesas();
		$rawData = $mesas->crearMesa($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear la mesa!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		} else if(strpos($requestContentType,'text/html') !== false){
			echo $rawData['data'];
		} else if(strpos($requestContentType,'application/xml') !== false){
            $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
            $xml->addChild($rawData['data']);
			$response = $xml->asXML();
			echo $response;
		}
    }
    function eliminarMesa($id)
    {
        $BaseRest = new BaseRest();
        $mesas = new Mesas();
		$rawData = $mesas->eliminarMesa($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo eliminar la mesa!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		} else if(strpos($requestContentType,'text/html') !== false){
			echo $rawData['data'];
		} else if(strpos($requestContentType,'application/xml') !== false){
            $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
            $xml->addChild($rawData['data']);
			$response = $xml->asXML();
			echo $response;
		}
    }
    function cambiarMesa($PedidoId,$MesaId)
    {
        $BaseRest = new BaseRest();
        $mesas = new Mesas();
		$rawData = $mesas->cambiarMesa($PedidoId,$MesaId);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo cambiar la mesa!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		} else if(strpos($requestContentType,'text/html') !== false){
			echo $rawData['data'];
		} else if(strpos($requestContentType,'application/xml') !== false){
            $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
            $xml->addChild($rawData['data']);
			$response = $xml->asXML();
			echo $response;
		}
    }
    function getAllMesas()
    {
        $BaseRest = new BaseRest();
        $mesas = new Mesas();
		$rawData = $mesas->getAllMesas();

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontraron mesas!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getMesa($id)
    {
        $BaseRest = new BaseRest();
        $mesas = new Mesas();
		$rawData = $mesas->getMesa($id);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontro la mesa!');		
		} else {
			$statusCode = 200;
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData, 0);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,0);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,0);
			echo $response;
		}
    }
    public function encodeHtml($responseData, $tipo) {
	
		$htmlResponse = "<table border='1'>";
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                    $htmlResponse .= "<tr><td>". $item['Mesa_id']. "</td><td>". $item['Nombre']. "</td></tr>";
            }
        }
        else
        {
            $htmlResponse .= "<tr><td>". $responseData['Mesa_id']. "</td><td>". $responseData['Nombre']. "</td></tr>";
        }
		$htmlResponse .= "</table>";
		return $htmlResponse;		
	}
	
	public function encodeJson($responseData, $tipo) {
		$jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
	public function encodeXml($responseData, $tipo) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                $xml->addChild($item['Mesa_id'], $item['Nombre']);
            }
        }
        else
        {
            $xml->addChild($responseData['Mesa_id'], $responseData['Nombre']);
        }
		return $xml->asXML();
	}
}
?>