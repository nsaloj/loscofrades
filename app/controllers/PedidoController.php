<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Pedido.php");
class PedidoController extends BaseRest
{
    function crearPedido($datos)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->crearPedido($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear el pedido!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		} else if(strpos($requestContentType,'text/html') !== false){
			echo $rawData['data'];
		} else if(strpos($requestContentType,'application/xml') !== false){
            $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
            $xml->addChild($rawData['data']);
			$response = $xml->asXML();
			echo $response;
		}
    }
    function pagoParcial($datos)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->pagoParcial($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo pagar el pedido!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function pagoTotal($datos)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->pagoTotal($datos);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo pagar el pedido!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
		}
    }
    function getPedidoMesa($Mesa_id)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getPedidoMesa($Mesa_id);

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo crear el pedido!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getEntregadoMesa($Mesa_id)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getEntregadoMesa($Mesa_id);

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se encontró entregados!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getPedidosCocina()
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getPedidosCocina();

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo econtrar el pedido!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getAll()
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getAll();

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar el pedido!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function getBebidas()
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getBebidas();

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar el pedido!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    function cambiarEstado($id,$estado)
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->cambiarEstado($id,$estado);

		if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo cambiar el estado!');
		} else {
			$statusCode = 200;
            $rawData = array('data' => $rawData);
		}

		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			echo json_encode($rawData);
        }
    }
    function getListos()
    {
        $BaseRest = new BaseRest();
        $pedido = new Pedido();
		$rawData = $pedido->getListos();

        if(empty($rawData)) {
			$statusCode = 404;
			$rawData = array('error' => 'No se pudo encontrar el pedido!');
		} else {
			$statusCode = 200;
            if(!empty($rawData['error'])) $rawData = array('data' => true);
		}
        //var_dump(json_encode($rawData));
		$requestContentType = $_SERVER['HTTP_ACCEPT'];
		$BaseRest->setHttpHeaders($requestContentType, $statusCode);
				
		if(strpos($requestContentType,'application/json') !== false){
			$response = self::encodeJson($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'text/html') !== false){
			$response = self::encodeHtml($rawData,1);
			echo $response;
		} else if(strpos($requestContentType,'application/xml') !== false){
			$response = self::encodeXml($rawData,1);
			echo $response;
		}
    }
    public function encodeHtml($responseData, $tipo) {
	
		$htmlResponse = "<table border='1'>";
        if($tipo == 1)
        {
            foreach($responseData as $data) {
                //$data = $responseData;
                $htmlResponse .= "<tr>
                                    <td>".$data['Pedido_id']."</td>
                                    <td>".$data['Comensales']."</td>
                                    <td>".$data['Fecha']."</td>
                                    <td>".$data['Mesa_id']."</td>
                                    <td>".$data['Usuario_id']."</td>
                                    <td>".$data['EstadoPedido_id']."</td>
                                    <td>".$data['Usuario_id']."</td>
                                  </tr>";
            
                foreach($data['Pedidos'] as $item) {
                        $htmlResponse .= "<tr>
                                            <td>".$item['Detalle_id']."</td>
                                            <td>".$item['Producto_id']."</td>
                                            <td>".$item['Nombre']."</td>
                                            <td>".$item['Producto_precio']."</td>
                                            <td>".$item['Comentario']."</td>
                                            <td>".$item['Cantidad']."</td>
                                            <td>".$item['EstadoDetalle_id']."</td>
                                          </tr>";
                }
            }
            
        }
        else
        {
            $htmlResponse .= "<tr><td>". $responseData['data']. "</td></tr>";
        }
		$htmlResponse .= "</table>";
		return $htmlResponse;		
	}
	
	public function encodeJson($responseData, $tipo) {
        $jsonResponse = json_encode($responseData);
		return $jsonResponse;		
	}
	
	public function encodeXml($responseData, $tipo) {
		$xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
        if($tipo == 1)
        {
            foreach($responseData as $item) {
                $xml->addChild($item['data']);
            }
        }
        else
        {
            $xml->addChild($responseData['data']);
        }
		return $xml->asXML();
	}
}
?>