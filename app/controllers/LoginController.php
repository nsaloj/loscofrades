<?php    
require_once("app/utils/BaseRest.php");
require_once("app/models/Usuario.php");
class LoginController extends BaseRest
{
    function Index()
    {
        return include('app/views/Login.php');
    }
    function Registro()
    {
        return include('app/views/Registro.php');
    }
    function loguear($datos)
    {
        $usr = new Usuario();
        $user = $usr->getLogin($datos);
        if($user)
        {
            $do = false;
            $userObj["id"] = $user['id'];
            if(empty($user["hash"]))
            {
                if($user["hash"] !== NULL && $user["hash"] !== "")
                {
                    $userObj["hash"] = $user['hash'];
                }
                else {
                    $do = true;
                }
            }
            else if(!empty($datos['recordar'])) $do = true;
            if($do === true)
            {
                $userObj["hash"] = self::str_random(30);
                $result = $usr->setHash($userObj);
                if($result === true)
                {
                    return $userObj;
                }
                else return false;
            }
            return $userObj;
        }
        else return false;
    }
    function getUsuario($datos)
    {
        $usr = new Usuario();
        return $respuesta = $usr->getUsuario($datos);
    }
    function existeSesion($datos)
    {
        $usr = new Usuario();
        return $respuesta = $usr->existeSesion($datos);
        return false;
    }
    function str_random($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}