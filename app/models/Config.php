<?php
class Config
{
    private $variables;
    private static $instancia;
 
    private function __construct()
    {
        $this->variables = array();
    }
 
    //Con set vamos guardando nuestras variables.
    public function set($name, $value)
    {
        if(!isset($this->variables[$name]))
        {
            $this->variables[$name] = $value;
        }
    }
 
    //Con get('nombre_de_la_variable') recuperamos un valor.
    public function get($name)
    {
        if(isset($this->variables[$name]))
        {
            return $this->variables[$name];
        }
    }
 
    public static function PSingleton()
    {
        if (!isset(self::$instancia)) {
            $c = __CLASS__;
            self::$instancia = new $c;
        }
 
        return self::$instancia;
    }
}