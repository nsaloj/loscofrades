<?php
class Categoria extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function crearCategoria($datos)
    {
        if($datos)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "INSERT INTO TB_Categoria (Categoria_id, Nombre, Padre_id) VALUES (?,?,?) ON DUPLICATE KEY UPDATE Nombre=VALUES(Nombre),Padre_id=VALUES(Padre_id)";
            $stmt = $this->_db->prepare($sql);
            $id = null;
            if(isset($datos['Categoria_id'])) $id = $datos['Categoria_id'];
            $stmt->bind_param('isi', $id,$datos['Nombre'],$datos['Padre_id']);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function getCategoria($id){
		$result = $this->_db->query('SELECT * FROM TB_Categoria  WHERE Categoria_id='.$id);
        $categoria = $result->fetch_array(MYSQLI_ASSOC); 
        $this->_db->close();
        return $categoria;
	}	
    public function eliminar($id)
    {
        if($id)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "DELETE FROM TB_Categoria WHERE Categoria_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('i', $id);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function getCategorias(){
        $result = $this->_db->query("SELECT * FROM TB_Categoria");
        $categorias = $result->fetch_all(MYSQLI_ASSOC);
        $this->_db->close();
        return $categorias;
    }
    
    public function getTop(){
        $result = $this->_db->query("SELECT * FROM TB_Categoria WHERE Padre_id IS NULL");
        $categorias = $result->fetch_all(MYSQLI_ASSOC);
        $this->_db->close();
        return $categorias;
    }
    
    public function getHijos(){
        $result = $this->_db->query("SELECT tc.Categoria_id as Categoria_id, tc.Nombre as Nombre, tcp.Nombre as Padre FROM TB_Categoria tc
        INNER JOIN TB_Categoria tcp ON tc.Padre_id = tcp.Categoria_id
        WHERE tc.Padre_id IS NOT NULL");
        $categorias = $result->fetch_all(MYSQLI_ASSOC);
        $this->_db->close();
        return $categorias;
    }
    
    public function getArbol(){
        $result = $this->_db->query("SELECT * FROM TB_Categoria WHERE Padre_id IS NULL");
                
        $arbol = array();
        while ($padre = $result->fetch_assoc()) {
            $temp = array(
                "Categoria_id" => $padre["Categoria_id"],
                "Nombre" => $padre["Nombre"],
                "Padre_id" => $padre["Padre_id"]
            );
            $hijos = $this->_db->query("SELECT * FROM TB_Categoria WHERE Padre_id= ".$padre["Categoria_id"]);
            while ($hijo = $hijos->fetch_assoc()) {
                $temp["Hijos"][] = array(
                    "Categoria_id" => $hijo["Categoria_id"],
                    "Nombre" => $hijo["Nombre"],
                    "Padre_id" => $hijo["Padre_id"]
                );
            }
            $arbol[] = $temp;
        }
        $this->_db->close();
        return $arbol;
    }
    
    public function getproductosCategoria($id){
        if($id){
            $result = $this->_db->query("SELECT * FROM TB_Productos WHERE Categoria_id=".$id);
            $productos = $result->fetch_all(MYSQLI_ASSOC); 
            $this->_db->close();
            return $productos;            
        }
        return false;
    }
    
    function debug() {
        $trace = debug_backtrace();
        $rootPath = dirname(dirname(__FILE__));
        $file = str_replace($rootPath, '', $trace[0]['file']);
        $line = $trace[0]['line'];
        $var = $trace[0]['args'][0];
        $lineInfo = sprintf('<div><strong>%s</strong> (line <strong>%s</strong>)</div>', $file, $line);
        $debugInfo = sprintf('<pre>%s</pre>', print_r($var, true));
        print_r($debugInfo);
    }
}
?>