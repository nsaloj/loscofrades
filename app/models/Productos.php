<?php
class Productos extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function crearProducto($datos)
    {
        if($datos)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "INSERT INTO TB_Productos (Producto_id, Nombre, Precio, Categoria_id) VALUES (?,?,?,?) ON DUPLICATE KEY UPDATE Nombre=VALUES(Nombre), Precio=VALUES(Precio),Categoria_id=VALUES(Categoria_id)";
            $stmt = $this->_db->prepare($sql);
            $id = null;
            if(isset($datos['Producto_id'])) $id = $datos['Producto_id'];
            $stmt->bind_param('isdi', $id, $datos['Nombre'],$datos['Precio'],$datos['Categoria_id']);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function getProducto($id){
		$result = $this->_db->query('SELECT * FROM TB_Productos  WHERE Producto_id='.$id);
        $categoria = $result->fetch_array(MYSQLI_ASSOC); 
        $this->_db->close();
        return $categoria;
	}
    public function eliminar($id)
    {
        if($id)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "DELETE FROM TB_Productos WHERE Producto_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('i', $id);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
	public function getAll(){
        $result = $this->_db->query("SELECT Producto_id, Nombre, Precio, Categoria_id FROM TB_Productos");
        $productos = $result->fetch_all(MYSQLI_ASSOC); 
        $this->_db->close();
        return $productos;        
    }	
    public function getTotal($datos){
        if($datos)
        {
            $filterBy = "";
            $filter = (isset($datos['filterBy']))? (!empty($datos['filterBy'])):false;
            $cont = 0;
            if($filter === true){
                foreach($datos['filterBy'] as $filtro){
                    if(!empty($filtro['campo']))
                    {
                        if(strpos(strtoupper($filtro['campo']), 'NOMBRE') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Nombre like '%".$filtro['valor']."%' ";
                            else $filterBy .= " AND TB_Productos.Nombre like '%".$filtro['valor']."%' ";
                            $cont = $cont +1;
                        }
                        else if(strpos(strtoupper($filtro['campo']), 'PRECIO') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Precio = '".$filtro['valor']."' ";
                            else $filterBy .= " AND TB_Productos.Precio = '".$filtro['valor']."' ";
                            $cont = $cont +1;
                        }
                        else if(strpos(strtoupper($filtro['campo']), 'CATEGORIA') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Categoria_id = '".$filtro['valor']."' ";
                            else $filterBy .= " AND TB_Productos.Categoria_id = '".$filtro['valor']."' ";
                            $cont = $cont +1;
                        }
                    }
                }
            }
            
            $result = $this->_db->query("SELECT COUNT(Producto_id) as Total FROM TB_Productos ".$filterBy);
            $total = [];
            while($ob = $result->fetch_assoc()) {
                $total['Total'] = $ob["Total"];
            }
            $this->_db->close();
            return $total;
        }
        else
        {
            $result = $this->_db->query("SELECT COUNT(Producto_id) as Total FROM TB_Productos");
            $total = [];
            while($ob = $result->fetch_assoc()) {
                $total['Total'] = $ob["Total"];
            }
            $this->_db->close();
            return $total;
        }
    }
    public function getFull($datos){
        if($datos)
        {
            $orderBy = "";
            $order = (isset($datos['orderBy']))? (!empty($datos['orderBy'])):false;
            if($order === true){
                if(strpos(strtoupper($datos['orderBy']), 'NOMBRE') !== false){
                    $orderBy = " ORDER BY TB_Productos.Nombre ";
                }
                else if(strpos(strtoupper($datos['orderBy']), 'PRECIO') !== false){
                    $orderBy = " ORDER BY TB_Productos.Precio ";
                }
                else if(strpos(strtoupper($datos['orderBy']), 'CATEGORIA') !== false){
                    $orderBy = " ORDER BY TB_Categoria.Nombre ";
                }
                if(strpos(strtoupper($datos['orderBy']), 'ASC') !== false && !empty($orderBy)){
                    $orderBy .= " ASC ";
                }
                else if(strpos(strtoupper($datos['orderBy']), 'DESC') !== false && !empty($orderBy)){
                    $orderBy .= " DESC ";
                }
            }
            
            
            $filterBy = "";
            $filter = (isset($datos['filterBy']))? (!empty($datos['filterBy'])):false;
            $cont = 0;
            if($filter === true){
                foreach($datos['filterBy'] as $filtro){
                    if(!empty($filtro['campo']))
                    {
                        if(strpos(strtoupper($filtro['campo']), 'NOMBRE') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Nombre like '%".$filtro['valor']."%' ";
                            else $filterBy .= " AND TB_Productos.Nombre like '%".$filtro['valor']."%' ";
                            $cont = $cont +1;
                        }
                        else if(strpos(strtoupper($filtro['campo']), 'PRECIO') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Precio = '".$filtro['valor']."' ";
                            else $filterBy .= " AND TB_Productos.Precio = '".$filtro['valor']."' ";
                            $cont = $cont +1;
                        }
                        else if(strpos(strtoupper($filtro['campo']), 'CATEGORIA') !== false){
                            if($cont == 0) $filterBy = " WHERE TB_Productos.Categoria_id = '".$filtro['valor']."' ";
                            else $filterBy .= " AND TB_Productos.Categoria_id = '".$filtro['valor']."' ";
                            $cont = $cont +1;
                        }
                    }
                }
            }
            
            
            $result = $this->_db->query("SELECT TB_Productos.Producto_id as Producto_id, TB_Productos.Nombre as Nombre, TB_Productos.Precio as Precio, TB_Productos.Categoria_id as Categoria_id, TB_Categoria.Nombre as Categoria, TB_Categoria.Padre_id as Padre_id 
            FROM TB_Productos
            INNER JOIN TB_Categoria ON TB_Productos.Categoria_id = TB_Categoria.Categoria_id ".
            $filterBy.
            $orderBy.                           
            " LIMIT ".$datos['inicio'].", ".$datos['cantidad']);
            $productos = $result->fetch_all(MYSQLI_ASSOC); 
            $this->_db->close();
            return $productos;
        }
        else{
            $result = $this->_db->query("SELECT TB_Productos.Producto_id as Producto_id, TB_Productos.Nombre as Nombre, TB_Productos.Precio as Precio, TB_Categoria.Categoria_id as Categoria_id, TB_Categoria.Nombre as Categoria, TB_Categoria.Padre_id as Padre_id 
            FROM TB_Productos
            INNER JOIN TB_Categoria ON TB_Productos.Categoria_id = TB_Categoria.Categoria_id");
            $productos = $result->fetch_all(MYSQLI_ASSOC); 
            $this->_db->close();
            return $productos;
        }
        return false;
    }	
}
?>