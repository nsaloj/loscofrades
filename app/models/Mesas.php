<?php
class Mesas extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function crearMesa($datos)
    {
        if($datos)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "INSERT INTO TB_Mesas (Mesa_id,Nombre) VALUES (?,?) ON DUPLICATE KEY UPDATE Nombre=VALUES(Nombre)";
            $stmt = $this->_db->prepare($sql);
            $id = null;
            if(isset($datos['Mesa_id'])) $id = $datos['Mesa_id'];
            $stmt->bind_param('is', $id, $datos['Nombre']);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function eliminarMesa($id)
    {
        if($id)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "DELETE FROM TB_Mesas WHERE Mesa_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('i', $id);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function cambiarMesa($pedido, $mesa)
    {
        if($pedido && $mesa)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "UPDATE TB_Pedido SET Mesa_id=? WHERE Pedido_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('ii', $mesa, $pedido);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            return $estado;
        }
        return false;
    }
	public function getAllMesas(){
        $result = $this->_db->query("SELECT TB_Mesas.Mesa_id as Mesa_id, TB_Mesas.Nombre as Nombre, TB_Pedido.Comensales as Comensales, TB_Pedido.EstadoPedido_id as EstadoPedido FROM TB_Mesas LEFT JOIN TB_Pedido ON TB_Pedido.Mesa_id = TB_Mesas.Mesa_id AND TB_Pedido.EstadoPedido_id = 1 ORDER BY TB_Mesas.Mesa_id, TB_Pedido.EstadoPedido_id DESC");
        $mesas = $result->fetch_all(MYSQLI_ASSOC);
        $this->_db->close();
        return $mesas;
    }
	public function getMesa($id){
		$result = $this->_db->query('SELECT TB_Mesas.Mesa_id as Mesa_id, TB_Mesas.Nombre as Nombre, TB_Pedido.Comensales as Comensales, TB_Pedido.EstadoPedido_id as EstadoPedido FROM TB_Mesas 
        LEFT JOIN TB_Pedido ON TB_Pedido.Mesa_id = TB_Mesas.Mesa_id AND TB_Pedido.EstadoPedido_id = 1 
        WHERE TB_Mesas.Mesa_id='.$id.' ORDER BY TB_Pedido.EstadoPedido_id DESC');
        $mesa = $result->fetch_array(MYSQLI_ASSOC); 
        $this->_db->close();
        return $mesa;
	}	
}
?>