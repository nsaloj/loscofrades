<?php
class Pedido extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function cambiarEstado($id,$estadoId)
    {
        if($id && $estadoId)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "UPDATE TB_DetallePedido SET EstadoDetalle_id=? WHERE Detalle_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('ii', $estadoId, $id);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            return $estado;
        }
        return false;
    }
    public function getBebidas(){
        $result = $this->_db->query("SELECT 
            TB_Pedido.Pedido_id as Pedido_id, TB_Pedido.Comensales as Comensales, TB_Pedido.Fecha as Fecha, TB_Pedido.Mesa_id as Mesa_id, TB_Pedido.Usuario_id as Usuario_id, TB_Pedido.EstadoPedido_id as EstadoPedido_id, TB_DetallePedido.Detalle_id, TB_DetallePedido.Cantidad as Cantidad,TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto, TB_Categoria.Padre_id as CategoriaPadre_id
            FROM TB_Pedido 
            INNER JOIN TB_DetallePedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            INNER JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            INNER JOIN TB_Categoria ON TB_Productos.Categoria_id = TB_Categoria.Categoria_id
            WHERE TB_Pedido.EstadoPedido_id = 1 AND TB_DetallePedido.EstadoDetalle_id <= 2 AND TB_Categoria.Padre_id = 2 GROUP BY TB_Pedido.Pedido_id, TB_DetallePedido.Detalle_id ORDER BY TB_DetallePedido.Fecha ASC");
        
            $pedidos = array();
            $pedidoOb= array();
            while($pedido = $result->fetch_assoc()) {
                
                $estado = (isset($pedidoOb["Pedido_id"]))? ($pedidoOb["Pedido_id"] !== $pedido["Pedido_id"]):false;
                
                $pedidoObj=array("Pedido_id"=>$pedido["Pedido_id"],"Comensales"=>$pedido["Comensales"],"Fecha"=>$pedido["Fecha"],               "Mesa_id"=>$pedido["Mesa_id"],"Usuario_id"=>$pedido["Usuario_id"],"EstadoPedido_id"=>$pedido["EstadoPedido_id"],
                    "Pedidos"=>[]);
                
                $detalle=array("Detalle_id"=>$pedido["Detalle_id"],"Producto_id"=>$pedido["Producto_id"],"Nombre"=>$pedido["Producto"],"Producto_precio"=>$pedido["Precio"],"Comentario"=>$pedido["Comentario"],"Cantidad"=>$pedido["Cantidad"],"EstadoDetalle_id"=>$pedido["EstadoDetalle_id"]);
                
                if(empty($pedidoOb) === true){
                    if(!empty($detalle['Detalle_id'])){
                        array_push($pedidoObj['Pedidos'], $detalle);
                        $pedidoOb = $pedidoObj;
                    }
                }
                else if($estado === false)
                {
                    if(!empty($detalle['Detalle_id'])){
                        if(empty($pedidoOb) === true) 
                        {
                            array_push($pedidoObj['Pedidos'], $detalle);
                        }
                        else
                        {                        
                            array_push($pedidoOb['Pedidos'], $detalle);
                        }
                    }
                }
                else if($estado === true)
                {
                    if(!empty($detalle['Detalle_id'])){
                        array_push($pedidos, $pedidoOb);
                        array_push($pedidoObj['Pedidos'], $detalle);
                        $pedidoOb = $pedidoObj;
                    }
                }
            }            
            if(!empty($pedidoOb))
            {
                array_push($pedidos, $pedidoOb);
            }
            $this->_db->close();
            return $pedidos;
    }
	public function getAll(){
        $result = $this->_db->query("SELECT 
            TB_Pedido.Pedido_id as Pedido_id, TB_Pedido.Comensales as Comensales, TB_Pedido.Fecha as Fecha, TB_Pedido.Mesa_id as Mesa_id, TB_Pedido.Usuario_id as Usuario_id, TB_Pedido.EstadoPedido_id as EstadoPedido_id, TB_DetallePedido.Detalle_id, TB_DetallePedido.Cantidad as Cantidad,TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto, TB_Categoria.Padre_id as CategoriaPadre_id
            FROM TB_Pedido 
            INNER JOIN TB_DetallePedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            INNER JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            INNER JOIN TB_Categoria ON TB_Productos.Categoria_id = TB_Categoria.Categoria_id
            WHERE TB_Pedido.EstadoPedido_id = 1 AND TB_DetallePedido.EstadoDetalle_id <= 2 AND TB_Categoria.Padre_id = 1 GROUP BY TB_Pedido.Pedido_id, TB_DetallePedido.Detalle_id ORDER BY TB_DetallePedido.Fecha ASC");
        
            $pedidos = array();
            $pedidoOb= array();
            while($pedido = $result->fetch_assoc()) {
                
                $estado = (isset($pedidoOb["Pedido_id"]))? ($pedidoOb["Pedido_id"] !== $pedido["Pedido_id"]):false;
                
                $pedidoObj=array("Pedido_id"=>$pedido["Pedido_id"],"Comensales"=>$pedido["Comensales"],"Fecha"=>$pedido["Fecha"],               "Mesa_id"=>$pedido["Mesa_id"],"Usuario_id"=>$pedido["Usuario_id"],"EstadoPedido_id"=>$pedido["EstadoPedido_id"],
                    "Pedidos"=>[]);
                
                $detalle=array("Detalle_id"=>$pedido["Detalle_id"],"Producto_id"=>$pedido["Producto_id"],"Nombre"=>$pedido["Producto"],"Producto_precio"=>$pedido["Precio"],"Comentario"=>$pedido["Comentario"],"Cantidad"=>$pedido["Cantidad"],"EstadoDetalle_id"=>$pedido["EstadoDetalle_id"]);
                
                if(empty($pedidoOb) === true){
                    if(!empty($detalle['Detalle_id'])){
                        array_push($pedidoObj['Pedidos'], $detalle);
                        $pedidoOb = $pedidoObj;
                    }
                }
                else if($estado === false)
                {
                    if(!empty($detalle['Detalle_id'])){
                        if(empty($pedidoOb) === true) 
                        {
                            array_push($pedidoObj['Pedidos'], $detalle);
                        }
                        else
                        {                        
                            array_push($pedidoOb['Pedidos'], $detalle);
                        }
                    }
                }
                else if($estado === true)
                {
                    if(!empty($detalle['Detalle_id'])){
                        array_push($pedidos, $pedidoOb);
                        array_push($pedidoObj['Pedidos'], $detalle);
                        $pedidoOb = $pedidoObj;
                    }
                }
            }            
            if(!empty($pedidoOb))
            {
                array_push($pedidos, $pedidoOb);
            }
            $this->_db->close();
            return $pedidos;
    }
    
    public function getListos(){
        
            $result = $this->_db->query("SELECT 
            TB_Pedido.Pedido_id as Pedido_id, TB_Pedido.Comensales as Comensales, TB_Pedido.Fecha as Fecha, TB_Mesas.Nombre as Mesa_nombre, TB_Pedido.Usuario_id as Usuario_id, TB_Pedido.EstadoPedido_id as EstadoPedido_id, TB_DetallePedido.Detalle_id, TB_DetallePedido.Cantidad as Cantidad,TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto
            FROM TB_DetallePedido 
            INNER JOIN TB_Pedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            INNER JOIN TB_Mesas ON TB_Pedido.Mesa_id = TB_Mesas.Mesa_id
            INNER JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            WHERE TB_Pedido.EstadoPedido_id = 1 AND TB_DetallePedido.EstadoDetalle_id = 3 ORDER BY TB_DetallePedido.Fecha ASC");
        
            $pedidos = array();
            $pedidoOb= array();
            while($pedido = $result->fetch_assoc()) {
                 $pedidoObj=array("Pedido_id"=>$pedido["Pedido_id"],"Comensales"=>$pedido["Comensales"],"Fecha"=>$pedido["Fecha"],"Mesa_id"=>$pedido["Mesa_nombre"],"Usuario_id"=>$pedido["Usuario_id"],"EstadoPedido_id"=>$pedido["EstadoPedido_id"],"Pedidos"=>[]);
            
                $detalle=array("Detalle_id"=>$pedido["Detalle_id"],"Producto_id"=>$pedido["Producto_id"],"Nombre"=>$pedido["Producto"],"Producto_precio"=>$pedido["Precio"],"Comentario"=>$pedido["Comentario"],"Cantidad"=>$pedido["Cantidad"],"EstadoDetalle_id"=>$pedido["EstadoDetalle_id"]);
                
                
                array_push($pedidoObj['Pedidos'], $detalle);

                array_push($pedidos, $pedidoObj);
            }    
            $this->_db->close();
            return $pedidos;      
    }
    
    public function getPedidoMesa($MesaId){
        if($MesaId)
        {
            $result = $this->_db->query('SELECT 
            TB_Pedido.Pedido_id as Pedido_id, TB_Pedido.Comensales as Comensales, TB_Pedido.Fecha as Fecha, TB_Pedido.Mesa_id as Mesa_id, TB_Pedido.Usuario_id as Usuario_id, TB_Pedido.EstadoPedido_id as EstadoPedido_id, TB_DetallePedido.Detalle_id, TB_DetallePedido.Cantidad as Cantidad, (TB_DetallePedido.Cantidad-COALESCE(SUM(TB_Cobro.Cantidad),0)) as noPagados,TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto, COALESCE(SUM(TB_Cobro.Cantidad),0) as Pagados
            FROM TB_Pedido 
            LEFT JOIN TB_DetallePedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            LEFT JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            LEFT JOIN TB_Cobro ON tb_cobro.Detalle_id = tb_detallepedido.Detalle_id
            WHERE TB_Pedido.Mesa_id='.$MesaId.' AND TB_Pedido.EstadoPedido_id = 1 
            GROUP BY tb_detallepedido.Detalle_id
            HAVING (TB_DetallePedido.Cantidad-COALESCE(SUM(TB_Cobro.Cantidad),0)) > 0
            ORDER BY TB_DetallePedido.Fecha ASC');
             
            $pedidos = array();
            $cont = 0;
            while($pedido = $result->fetch_assoc()) {
                if($cont === 0) $pedidos = array("Pedido_id"=>$pedido["Pedido_id"],"Comensales"=>$pedido["Comensales"],"Fecha"=>$pedido["Fecha"],"Mesa_id"=>$pedido["Mesa_id"],"Usuario_id"=>$pedido["Usuario_id"],"EstadoPedido_id"=>$pedido["EstadoPedido_id"],"Pedidos"=>[]);
                
                //$pedidos['Pedido'] = $user["Usuario_id"];
                //$userObj['hash'] = $user["Session_id"];
                $pedidoObj = array("Detalle_id"=>$pedido["Detalle_id"],"Producto_id"=>$pedido["Producto_id"],"Producto"=>$pedido["Producto"],"Producto_precio"=>$pedido["Precio"],"Comentario"=>$pedido["Comentario"],"noPagados"=>$pedido["noPagados"],"Cantidad"=>$pedido["Cantidad"],"EstadoDetalle_id"=>$pedido["EstadoDetalle_id"]);
                array_push($pedidos['Pedidos'], $pedidoObj);
                $cont = 1;
            }
            //$pedidos = $result->fetch_all(MYSQLI_ASSOC); 
            $this->_db->close();
            //var_dump($pedidos);
            return $pedidos;
        }
        return false;
    }
    public function getEntregadoMesa($MesaId){
        if($MesaId)
        {
            $result = $this->_db->query('SELECT 
            TB_Pedido.Pedido_id as Pedido_id, TB_Pedido.Comensales as Comensales, TB_Pedido.Fecha as Fecha, TB_Pedido.Mesa_id as Mesa_id, TB_Pedido.Usuario_id as Usuario_id, TB_Pedido.EstadoPedido_id as EstadoPedido_id, TB_DetallePedido.Detalle_id, TB_DetallePedido.Cantidad as Cantidad, (TB_DetallePedido.Cantidad-COALESCE(SUM(TB_Cobro.Cantidad),0)) as noPagados,TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto, COALESCE(SUM(TB_Cobro.Cantidad),0) as Pagados
            FROM TB_Pedido 
            LEFT JOIN TB_DetallePedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            LEFT JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            LEFT JOIN TB_Cobro ON tb_cobro.Detalle_id = tb_detallepedido.Detalle_id
            WHERE TB_Pedido.Mesa_id='.$MesaId.' AND TB_Pedido.EstadoPedido_id = 1 AND TB_DetallePedido.EstadoDetalle_id = 4
            GROUP BY tb_detallepedido.Detalle_id
            
            ORDER BY TB_DetallePedido.Fecha ASC');
             
            $pedidos = array();
            $cont = 0;
            while($pedido = $result->fetch_assoc()) {
                if($cont === 0) $pedidos = array("Pedido_id"=>$pedido["Pedido_id"],"Comensales"=>$pedido["Comensales"],"Fecha"=>$pedido["Fecha"],"Mesa_id"=>$pedido["Mesa_id"],"Usuario_id"=>$pedido["Usuario_id"],"EstadoPedido_id"=>$pedido["EstadoPedido_id"],"Pedidos"=>[]);
                
                //$pedidos['Pedido'] = $user["Usuario_id"];
                //$userObj['hash'] = $user["Session_id"];
                $pedidoObj = array("Detalle_id"=>$pedido["Detalle_id"],"Producto_id"=>$pedido["Producto_id"],"Producto"=>$pedido["Producto"],"Producto_precio"=>$pedido["Precio"],"Comentario"=>$pedido["Comentario"],"noPagados"=>$pedido["noPagados"],"Cantidad"=>$pedido["Cantidad"],"EstadoDetalle_id"=>$pedido["EstadoDetalle_id"]);
                array_push($pedidos['Pedidos'], $pedidoObj);
                $cont = 1;
            }
            //$pedidos = $result->fetch_all(MYSQLI_ASSOC); 
            $this->_db->close();
            //var_dump($pedidos);
            return $pedidos;
        }
        return false;
    }
    public function crearPedido($datos)
    {
        if($datos)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "INSERT INTO TB_Pedido (Pedido_id, Comensales, Fecha, Mesa_id, Usuario_id, EstadoPedido_id) VALUES (?,?, NOW(), ?, ?, ?) ON DUPLICATE KEY UPDATE Comensales=VALUES(Comensales)";
            $id = null;
            if(isset($datos['Pedido_id'])) $id = $datos['Pedido_id'];
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('iiiii', $id, $datos['Comensales'],$datos['Mesa_id'], $datos['Usuario_id'], $datos['EstadoPedido_id']);
            
            if ($stmt->execute() === TRUE) {
                $aux = ($id == null)? $this->_db->insert_id:$id;
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: <br>" . $this->_db->error;
                $estado = $error;
                //return json_encode($error);
            }
            
            
            $pedidos = $datos['Pedidos'];
            foreach($pedidos as $pedido)
            {
                $idDetalle = $pedido['Detalle_id'];
                if($pedido['EstadoDetalle_id'] > 0){
                    $sql  = "INSERT INTO TB_DetallePedido (Detalle_id, Pedido_id, Producto_id, Cantidad, Precio, Comentario, EstadoDetalle_id,Fecha) VALUES (?, ?, ?, ?, ?, ?, ?,NOW()) ON DUPLICATE KEY UPDATE EstadoDetalle_id=VALUES(EstadoDetalle_id), Comentario=VALUES(Comentario)";
                    //?) ON DUPLICATE KEY UPDATE Cantidad=VALUES(Cantidad), Comentario=VALUES(Comentario)";
                    
                    
                    $idDetalleM = ($idDetalle)? $idDetalle:'a';
                    if(preg_match('/[^0-9]/', $idDetalleM)) 
                    {
                        $idDetalleM = null;
                    }
                    
                    $eid = $idDetalleM;
                    
                    $stmt = $this->_db->prepare($sql);
                    $stmt->bind_param('iiiidsi', $eid, $aux,$pedido['Producto_id'], $pedido['Cantidad'],$pedido['Producto_precio'],$pedido['Comentario'], $pedido['EstadoDetalle_id']);
                    if ($stmt->execute() === TRUE) {
                        $estado = true;
                    } else {
                        $estado = false;
                        $error['error'] = "Error: " . $this->_db->error;
                        $estado = $error;
                    }
                }
                else{                    
                    $sqlD  = "DELETE FROM TB_DetallePedido WHERE Detalle_id=?";
                    $stmt = $this->_db->prepare($sqlD);
                    $stmt->bind_param('i', $idDetalle);
                    if ($stmt->execute() === TRUE) {
                        $estado = true;
                    } else {
                        $estado = false;
                        $error['error'] = "Error: " . $this->_db->error;
                        $estado = $error;
                    }
                }
            }
            
            if ($estado === true){
                if(!$this->_db->commit()) {
                    $estado = false;
                    $error['error'] = "Error: >" . $this->_db->error;
                    $estado = $error;
                }
            }
           /*
            $estado = true;
            $this->_db->autocommit(FALSE);
            $estado = $this->_db->query("INSERT INTO TB_Pedido (Comensales, Fecha, Mesa_id, Usuario_id, EstadoPedido_id) VALUES 
            (2, NOW(), 3, 1, "+$datos['EstadoPedido_id']+" )");
            
            if ($estado === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: <br>" . $this->_db->error;
            }*/
            /* Consignar la transacción */
            /*if (!$this->_db->commit()) {
                $estado = false;
            }
            $this->_db->close();*/
            return $estado;
        }
        return false;
    }
    
    public function pagoParcial($datos)
    {
        if($datos && Auth::user())
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            
            if($datos['TOTAL'] === 'si')
            {
                $sql  = "UPDATE TB_Pedido SET EstadoPedido_id=5 WHERE Pedido_id=?";
                $stmt = $this->_db->prepare($sql);
                $stmt->bind_param('i', $datos['Pedido_id']);

                if ($stmt->execute() === TRUE) {
                    $estado = true;
                } else {
                    return $this->_db->error;
                }
            }
            
            
            if(isset($datos['Pedidos']))
            {
                $pedidos = $datos['Pedidos'];
                foreach($pedidos as $pedido)
                {                   
                    $sql  = "INSERT INTO TB_Cobro (Cobro_id, Detalle_id, Cantidad, Fecha, Usuario_id) VALUES (?,?, ?,NOW(),?) ON DUPLICATE KEY UPDATE Cobro_id=VALUES(Cobro_id)";
                    $id = null;
                    if(isset($pedido['Cobro_id'])) $id = $pedido['Cobro_id'];
                    $stmt = $this->_db->prepare($sql);
                    $stmt->bind_param('iiii', $id, $pedido['Detalle_id'],$pedido['Cantidad'],Auth::user());


                    if ($stmt->execute() === TRUE) {
                        $estado = true;
                    } else {
                        return $this->_db->error;
                    }
                }
            }
            if ($estado === true){
                if(!$this->_db->commit()) {
                    return $this->_db->error;
                }
            }
            return $estado;
                
        }
        return false;
    }
}
?>