<?php
class Usuario extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function getLogin($datos){
        $result = $this->_db->query("SELECT Usuario_id, Session_id FROM TB_Usuarios WHERE Usuario='".$datos["user"]."' AND Contrasenia='".md5($datos["password"])."'");
        $userObj = [];
        while($user = $result->fetch_assoc()) {
            $userObj['id'] = $user["Usuario_id"];
            $userObj['hash'] = $user["Session_id"];
        }
        
        return $userObj;       
    }
    public function existeSesion($datos){
        if($datos)
        {
            if($datos['userHash'])
            {
                $result = $this->_db->query("SELECT Usuario FROM TB_Usuarios WHERE Usuario_id='".$datos["userId"]."' AND Session_id='".$datos["userHash"]."'");
            }
            else{
                $result = $this->_db->query("SELECT Usuario FROM TB_Usuarios WHERE Usuario_id='".$datos["userId"]."'");
            }
            $userObj = $result->fetch_array(); 
            $result = false;
            if(!empty($userObj))
            {
                $result = true;
            }
            return $result;
        }
        return false;
    }
    public function existeUser($datos){
        if($datos)
        {
            $result = $this->_db->query("SELECT Usuario FROM TB_Usuarios WHERE Usuario='".$datos['user']."' AND Contrasenia='".md5($datos['password'])."'");
            $userObj = $result->fetch_array(); 
            $result = false;
            if(!empty($userObj))
            {
                $result = true;
            }
            return $result;
        }
        return false;
    }
	public function setHash($user){
        return $this->_db->query("UPDATE TB_Usuarios set Session_id='".$user["hash"]."' WHERE Usuario_id='".$user['id']."'");
    }
    public function getUsuario($id){
		$result = $this->_db->query('SELECT TB_Usuarios.Usuario_id as Usuario_id, TB_Usuarios.Usuario as Usuario, TB_Usuarios.Nombre as Nombre, TB_Usuarios.Apellidos as Apellidos, TB_Usuarios.Rol_id as Rol_id, TB_Roles.Nombre as Rol
        FROM TB_Usuarios 
        INNER JOIN TB_Roles ON TB_Roles.Rol_id = TB_Usuarios.Rol_id
        WHERE Usuario_id='.$id);
        $categoria = $result->fetch_array(MYSQLI_ASSOC); 
        $this->_db->close();
        return $categoria;
	}
    
    public function getUsuarios(){
		$result = $this->_db->query('SELECT TB_Usuarios.Usuario_id as Usuario_id, TB_Usuarios.Usuario as Usuario, TB_Usuarios.Nombre as Nombre, TB_Usuarios.Apellidos as Apellidos, TB_Usuarios.Rol_id as Rol_id, TB_Roles.Nombre as Rol
        FROM TB_Usuarios 
        INNER JOIN TB_Roles ON TB_Roles.Rol_id = TB_Usuarios.Rol_id WHERE TB_Usuarios.Rol_id != 1');
        $categoria = $result->fetch_all(MYSQLI_ASSOC); 
        $this->_db->close();
        return $categoria;
	}
    
    public function getSecciones($Rolid){
        $secciones = [array("sitio"=>"comedor","permiso"=>[1,2,4]),
                      array("sitio"=>"cocina","permiso"=>[1,2,3]),
                      array("sitio"=>"suministros","permiso"=>[0]),
                      array("sitio"=>"productos","permiso"=>[1,2,3,4]),
                      array("sitio"=>"informes","permiso"=>[1]),
                      array("sitio"=>"usuarios","permiso"=>[1]),
                      array("sitio"=>"ajustes","permiso"=>[0]),
                     ];
        //,"cocina":[1,2,3],"productos":[1,2,3],"suministros":[1,2,3],"informes":[1],"usuarios":[1],"ajustes":[0]);
        $permiso = [];
        foreach($secciones as $seccion){
            if (in_array($Rolid, $seccion["permiso"])){
                array_push($permiso, $seccion["sitio"]);
            }
        }
        
        if(sizeof($permiso)> 0)return $permiso;
        
        return false;
    }
    
    
    
    /*WS*/
    public function actualizarDatos($datos)
    {
        if($datos)
        {
            $estado = false;            
            
            $this->_db->autocommit(FALSE);
            if(empty($datos['newpassword']))
            {
                $sql  = "UPDATE TB_Usuarios set Usuario=?,Nombre=?, Apellidos=? WHERE Usuario_id=?";
                $stmt = $this->_db->prepare($sql);
                $stmt->bind_param('sssi', $datos['user'],$datos['nombre'],$datos['apellidos'],$datos['user_id']);
            }
            else
            {
                if(self::existeUser($datos) === true)
                {
                    $sql  = "UPDATE TB_Usuarios set Usuario=?,Nombre=?, Apellidos=?,Contrasenia=? WHERE Usuario_id=? and Contrasenia=?";
                    $stmt = $this->_db->prepare($sql);
                    $stmt->bind_param('ssssis',$datos['user'],$datos['nombre'],$datos['apellidos'],md5($datos['newpassword']),$datos['user_id'],md5($datos['password']));
                }
                else
                {
                    return false;
                }
            }
            
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    public function crearUsuario($datos)
    {
        if($datos)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "INSERT INTO TB_Usuarios (Usuario_id, Usuario,Nombre, Apellidos, Contrasenia, Rol_id) VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE Usuario=VALUES(Usuario), Nombre=VALUES(Nombre), Apellidos=VALUES(Apellidos),Rol_id=VALUES(Rol_id)";
            $stmt = $this->_db->prepare($sql);
            $id = null;
            if(isset($datos['Usuario_id'])) $id = $datos['Usuario_id'];
            
            $contrasenia = null;
            if(isset($datos['Contrasenia']))$contrasenia = $datos['Contrasenia'];
            
            $stmt->bind_param('issssi', $id, $datos['Usuario'],$datos['Nombre'],$datos['Apellidos'],md5($contrasenia),$datos['Rol_id']);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }    
    
    public function eliminar($id)
    {
        if($id)
        {
            $estado = true;
            $this->_db->autocommit(FALSE);
            $sql  = "DELETE FROM TB_Usuarios WHERE Usuario_id=?";
            $stmt = $this->_db->prepare($sql);
            $stmt->bind_param('i', $id);
            if ($stmt->execute() === TRUE) {
                $estado = true;
            } else {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            if(!$this->_db->commit()) {
                $estado = false;
                $error['error'] = "Error: " . $this->_db->error;
                $estado = $error;
            }
            
            $this->_db->close();
            return $estado;
        }
        return false;
    }
    
    public function getRoles(){
        $aux = false;
        if(Auth::getUser())
        {
            if(Auth::getUser()['Rol_id'] === 1) $aux = true;
        }
        if($aux === false) $result = $this->_db->query('SELECT * FROM TB_Roles');
        else  $result = $this->_db->query('SELECT * FROM TB_Roles WHERE Rol_id != 1');
        
        $categoria = $result->fetch_all(MYSQLI_ASSOC); 
        $this->_db->close();
        return $categoria;
	}
    function str_random($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
?>