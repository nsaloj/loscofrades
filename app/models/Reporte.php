<?php
class Reporte extends Modelo 
{
    public function __construct() 
    { 
        parent::__construct(); 
    }
    
    public function getVentas($datos){
        //var_dump($datos);
        if(!empty($datos))
        {
            if(!empty($datos["fecha_inicio"]))
            {
                $result = $this->_db->query("SELECT DATE_FORMAT(c.Fecha,'%d-%m-%Y') as Fecha, sum(p.Precio*c.Cantidad) TOTAL from TB_Cobro as c INNER JOIN TB_DetallePedido dp on (dp.Detalle_id=c.Detalle_id) 
                                    INNER JOIN TB_Productos p on (dp.Producto_id = p.Producto_id) 
                                    INNER JOIN TB_Pedido pd on (dp.Pedido_id = pd.Pedido_id)
                                    WHERE CAST(c.Fecha AS DATE)>='".$datos["fecha_inicio"]."' and CAST(c.Fecha AS DATE)<= '".$datos["fecha_fin"]."' and pd.EstadoPedido_id = 5 GROUP BY DATE_FORMAT(c.Fecha,'%m-%d-%Y') ORDER BY TOTAL ".$datos['Dir']);
            }
            else{
                $result = $this->_db->query("SELECT DATE_FORMAT(c.Fecha,'%d-%m-%Y') as Fecha, (p.Precio*sum(c.Cantidad)) TOTAL from TB_Cobro as c INNER JOIN TB_DetallePedido dp on (dp.Detalle_id=c.Detalle_id) 
                                    INNER JOIN TB_Productos p on (dp.Producto_id = p.Producto_id) 
                                    INNER JOIN TB_Pedido pd on (dp.Pedido_id = pd.Pedido_id)
                                    WHERE pd.EstadoPedido_id = 5 GROUP BY DATE_FORMAT(c.Fecha,'%m-%d-%Y') ORDER BY TOTAL ".$datos['Dir']);
            }
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            return $resultados;
            
            
        }
        else false;
    }
    public function getProductos($datos){
        //var_dump($datos);
        if(!empty($datos))
        {            
           if(!empty($datos["fecha_inicio"]))
            {
                $result = $this->_db->query("SELECT p.Producto_id,p.Nombre,sum(c.Cantidad) Cantidad,p.Precio, p.Precio*sum(c.Cantidad) Total from TB_Productos p 
                INNER JOIN TB_DetallePedido as dp on (p.Producto_id=dp.Producto_id) 
                INNER JOIN TB_Pedido as pd on (dp.Pedido_id=pd.Pedido_id) 
                INNER JOIN TB_Cobro as c on (dp.Detalle_id=c.Detalle_id) 
                WHERE CAST(c.Fecha AS DATE)>='".$datos["fecha_inicio"]."' and CAST(c.Fecha AS DATE)<= '".$datos["fecha_fin"]."' 
                GROUP BY p.Producto_id
                ORDER BY TOTAL ".$datos['Dir']." LIMIT 0,25");
            }
            else{
                $result = $this->_db->query("SELECT p.Producto_id,p.Nombre,sum(c.Cantidad) Cantidad,p.Precio, p.Precio*sum(c.Cantidad) Total from TB_Productos p 
                INNER JOIN TB_DetallePedido as dp on (p.Producto_id=dp.Producto_id) 
                INNER JOIN TB_Pedido as pd on (dp.Pedido_id=pd.Pedido_id) 
                INNER JOIN TB_Cobro as c on (dp.Detalle_id=c.Detalle_id) 
                GROUP BY p.Producto_id 
                ORDER BY TOTAL ".$datos['Dir']." LIMIT 0,25");
            }
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            return $resultados;
            
            
        }
        else false;
    }    
    public function getAfluencia($datos){
        //var_dump($datos);
        if(!empty($datos))
        {            
            setlocale (LC_TIME, "es_ES");
            if(!empty($datos["fecha_inicio"]))
            {
                $result = $this->_db->query("SELECT DATE_FORMAT(fecha,'%d-%m-%Y') as Fecha, DATE_FORMAT(fecha,'%W') as Dia, DATE_FORMAT(fecha,'%l %p') AS Hora, sum(Comensales) as Personas FROM TB_Pedido WHERE CAST(fecha AS DATE)>='".$datos["fecha_inicio"]."' and CAST(fecha AS DATE)<= '".$datos["fecha_fin"]."' GROUP BY DATE_FORMAT(fecha,'%H') ORDER BY sum(comensales) ".$datos['Dir']);
                
                /*
                $result = $this->_db->query("SELECT DATE_FORMAT(fecha,'%W') as Dia, 
                DATE_FORMAT(fecha,'%H') AS Hora, SUM(Comensales) as Personas 
                FROM TB_Pedido 
                WHERE CAST(fecha AS DATE)>='".$datos["fecha_inicio"]."' and CAST(fecha AS DATE)<= '".$datos["fecha_fin"]."'
                GROUP BY Dia, Hora
                HAVING Hora = (SELECT DATE_FORMAT(fecha,'%H')as Hora From TB_Pedido WHERE CAST(fecha AS DATE) = '2016-06-07' 
                GROUP BY DATE_FORMAT(fecha,'%H') order by SUM(Comensales) DESC LIMIT 1)
                ORDER BY SUM(Comensales)");
                */
            }
            else{
                $result = $this->_db->query("SELECT DATE_FORMAT(fecha,'%d-%m-%Y') as Fecha, DATE_FORMAT(fecha,'%W') as Dia, DATE_FORMAT(fecha,'%H') AS Hora, sum(Comensales) as Personas FROM TB_Pedido GROUP BY DATE_FORMAT(fecha,'%H') ORDER BY sum(comensales) ".$datos['Dir']);
            }
            
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            //echo "wayauaya";
            //var_dump($resultados);
            return $resultados;
            
            
        }
        else false;
    }
    
    public function getTotalVendido(){
        $result = $this->_db->query("SELECT SUM(dp.Precio*c.Cantidad) TOTAL 
        from TB_Cobro as c 
        INNER JOIN TB_DetallePedido dp on (dp.Detalle_id=c.Detalle_id)");
        $resultados = $result->fetch_assoc();
        $this->_db->close();
        return $resultados;
    }
    public function getTotalProductos(){
        
    }
    public function getVentasFecha($fecha){
        //var_dump($datos);
        if(!empty($fecha))
        {
            $result = $this->_db->query("SELECT DATE_FORMAT(c.Fecha,'%d-%m-%Y') as Fecha, DATE_FORMAT(c.Fecha,'%l:%i %p') as Hora, CONCAT ( CONCAT (u.Nombre,' '), u.Apellidos) as Usuario, pd.Pedido_id as Pedido_id ,sum(dp.Precio*c.Cantidad) TOTAL 
            FROM TB_Cobro as c 
            INNER JOIN TB_DetallePedido dp on (dp.Detalle_id=c.Detalle_id)
            INNER JOIN TB_Productos p on (dp.Producto_id = p.Producto_id)
            INNER JOIN TB_Pedido pd on (dp.Pedido_id = pd.Pedido_id)
            INNER JOIN TB_Usuarios u on (u.Usuario_id = pd.Usuario_id)
            WHERE CAST(c.Fecha AS DATE) = '".$fecha."' AND pd.EstadoPedido_id = 5 
            GROUP BY pd.Pedido_id
            ORDER BY pd.Fecha ASC
            ");
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            return $resultados;
            
        }
        else false;
    }
    public function getVenta($id){
        //var_dump($datos);
        if(!empty($id))
        {
            $result = $this->_db->query("SELECT 
            CONCAT ( CONCAT (u.Nombre,' '), u.Apellidos) as Usuario, sum(TB_DetallePedido.Cantidad) as Cantidad, TB_DetallePedido.Precio as Precio, TB_DetallePedido.Comentario as Comentario, TB_DetallePedido.EstadoDetalle_id as EstadoDetalle_id,  TB_DetallePedido.Producto_id as Producto_id, TB_Productos.Nombre as Producto, sum(TB_DetallePedido.Cantidad * TB_DetallePedido.Precio) as Total
            FROM TB_Pedido 
            LEFT JOIN TB_DetallePedido ON TB_Pedido.Pedido_id = TB_DetallePedido.Pedido_id
            LEFT JOIN TB_Productos ON TB_Productos.Producto_id = TB_DetallePedido.Producto_id
            LEFT JOIN TB_Cobro ON tb_cobro.Detalle_id = tb_detallepedido.Detalle_id
            LEFT JOIN TB_Usuarios u ON TB_Cobro.Usuario_id = u.Usuario_id
            WHERE TB_Pedido.Pedido_id = ".$id."
            GROUP BY TB_Productos.Producto_id
            ORDER BY TB_DetallePedido.Fecha ASC");
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            return $resultados;
            
        }
        else false;
    }
    /*FIN Informes*/
    
    
	public function getInformeVentas($datos){
        if($datos)
        {            
            $result = $this->_db->query("SELECT p.Producto_id, p.Nombre, SUM(c.Cantidad) Cantidad, p.Precio,
                                    p.Precio*sum(c.Cantidad) Total
                                    FROM TB_Productos p
                                    INNER JOIN TB_DetallePedido AS dp on (p.Producto_id=dp.Producto_id)
                                    INNER JOIN TB_Pedido AS pd ON (dp.Pedido_id=pd.Pedido_id)
                                    INNER JOIN TB_Cobro AS c ON (dp.Detalle_id=c.Detalle_id)
                                    WHERE CAST(c.Fecha AS DATE)>= ".$datos["fecha_inicio"]." AND
                                    CAST(c.Fecha AS DATE)<= ".$datos["fecha_fin"]." AND pd.EstadoPedido_id = 5 
                                    GROUP BY p.Producto_id,
                                    p.Nombre,
                                    p.Precio,
                                    dp.Cantidad
                                    ORDER BY Total desc");
            $resultados = $result->fetch_all(MYSQLI_ASSOC);
            $this->_db->close();
            return $resultados;
        }
        else false;
    }
}
?>