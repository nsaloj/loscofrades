<?php
$config = Config::PSingleton();
 
$config->set('DirControladores', 'app/controllers/');
$config->set('DirModelos', 'app/models/');
$config->set('DirViews', 'app/views/');
 
$config->set('dbhost', 'localhost');
$config->set('dbname', 'loscofrades');
$config->set('dbuser', 'root');
$config->set('dbpass', '');
?>