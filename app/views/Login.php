<?php 
    $msj = Flash::mensaje(['nombre'=>'login_resp']); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>LosCofrades | Login</title>

    <!-- Iniciar estilos referencias-->
    <link href="assets/app/css/OpenSans.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/app/css/normalize.css" rel="stylesheet" type="text/css" />
    <!-- CSS Personalizado -->
    <link href="assets/app/css/componentes.css" rel="stylesheet" type="text/css" />
    <link href="assets/app/css/login.css" rel="stylesheet" type="text/css" />
    <!-- FIN CSS Personalizado -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
    <div class="contenedor">
        <div class="login-box">
            <div class="logotipo">
                <img src="assets/images/logotipo.png" width="220"/>                
            </div>
            <form class="form-acceder" action="acceder" method="POST">
                <div class="alerta-box error <?php if(!isset($msj)){echo 'hide';}?>">
                   <div class="icono">
                       <i class="fa fa-exclamation"></i>
                   </div>
                   <p class="contenido">
                        <?php 
                            echo $msj['mensaje'];
                        ?>
                    </p>                    
                    <button class="cerrar boton-class"><span aria-hidden="true">&times;</span></button>
                </div>
                <input name="usuario" type="text" id="inputEmail" class="control-input" placeholder="Escriba su usario" required autofocus>
                <input name="contrasenia" type="password" id="inputPassword" class="control-input" placeholder="Escriba su contraseña" required>                
                <div class="checkbox">
                    <label>
                    <input type="checkbox" value="true" name="recordar"> Recordarme
                    </label>
                </div>
                <button class="boton-class boton-entrar" type="submit">Acceder</button>
            </form>
        </div>
    </div>
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/app/js/componentes.js" type="text/javascript"></script>
</body>
</html>