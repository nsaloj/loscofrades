<!DOCTYPE html>
<html lang="es" data-ng-app="CofradesApp">
    <head>
        <meta charset="UTF-8">
        <title>LosCofrades</title>
        
        <!-- Referencias y scripts-->
        <link href="assets/app/css/OpenSans.css" rel="stylesheet" type="text/css" />
        <link href='assets/app/css/Montserrat.css' rel='stylesheet' type='text/css'>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/app/css/normalize.css" rel="stylesheet" type="text/css" />
        <!-- CSS Personalizado -->
        <link href="assets/app/css/componentes.css" rel="stylesheet" type="text/css" />
        <link href="assets/app/css/global.css" rel="stylesheet" type="text/css" />
        <link href="assets/app/css/directivas.css" rel="stylesheet" type="text/css" />
        <link href="bower_components\ng-dialog\css\ngDialog.css" rel="stylesheet" type="text/css" />
        <link href="bower_components\ng-dialog\css\ngDialog-theme-plain.css" rel="stylesheet" type="text/css" />
        <link href="bower_components\ng-dialog\css\ngDialog-theme-default.css" rel="stylesheet" type="text/css" />
        <!-- FIN CSS Personalizado -->
        
        <link rel="shortcut icon" href="favicon.ico" />
        <meta name="viewport" content="width=device-width"/>
    </head>
    <body data-ng-controller="AppController">
       <div ng-spinner-bar class="spinner">      
          <div class="double-bounce1"></div>
          <div class="double-bounce2"></div>
        </div>       
        
        <div data-ng-include=" 'assets/app/plantillas/header.html' ">
        </div>        
        <div class="layout-vertical" ui-view></div>

        <!-- INICIO PLUGINS JQUERY -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="assets/app/js/componentes.js" type="text/javascript"></script>
        <!-- FIN PLUGINS JQUERY-->
        <!-- INICIO ANGULARJS PLUGINS -->
        <!--<script src="assets/global/plugins/angularjs/angular.min.js"></script>-->
        <script src="bower_components/angular/angular.min.js"></script>
        <script src="assets/global/plugins/angularjs/angular-route.min.js"></script>
        <script src="assets/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>        
        <script src="assets/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="bower_components/ngSocket/dist/ngSocket.js"></script>
        <script src="bower_components\ng-dialog\js\ngDialog.js"></script>
        <script src="bower_components\angular-audio\app\angular.audio.js"></script>
        <!-- FIN PLUGINS ANGULARJS-->
        <!-- Inicio Scripts APP -->
        <script src="assets/app/angular/inicio.js" type="text/javascript"></script>        
        <script src="assets/app/angular/rutas.js" type="text/javascript"></script>   
        <script src="assets/app/angular/factory.js" type="text/javascript"></script>
        <script src="assets/app/angular/servicios.js" type="text/javascript"></script>
        <script src="assets/app/angular/directivas.js" type="text/javascript"></script>
        <!-- FIN Scripts APP -->        
    </body>
<!-- END BODY -->
</html>