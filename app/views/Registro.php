<?php 
    $msj = Flash::mensaje(['nombre'=>'registro_resp']); 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>LosCofrades | Login</title>

    <!-- Iniciar estilos referencias-->
    <link href="assets/app/css/OpenSans.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/app/css/normalize.css" rel="stylesheet" type="text/css" />
    <!-- CSS Personalizado -->
    <link href="assets/app/css/componentes.css" rel="stylesheet" type="text/css" />
    <link href="assets/app/css/login.css" rel="stylesheet" type="text/css" />
    <!-- FIN CSS Personalizado -->
    <link rel="shortcut icon" href="favicon.ico" />
</head>
<body>
    <div class="contenedor">
        <div class="login-box">
            <div class="logotipo">
                <img src="assets/images/logotipo.png" width="220"/>                
            </div>
            <form class="form-acceder" action="registro" method="POST">
                <div class="alerta-box error <?php if(!isset($msj)){echo 'hide';}?>">
                   <div class="icono">
                       <i class="fa fa-exclamation"></i>
                   </div>
                   <p class="contenido">
                        <?php 
                            echo $msj['mensaje'];
                        ?>
                    </p>                    
                    <button class="cerrar boton-class"><span aria-hidden="true">&times;</span></button>
                </div>
                <label>Usuario:</label>
                <input name="usuario" type="text" id="inputEmail" class="control-input" placeholder="Escriba su usario" required autofocus>
                <label>Contraseña:</label>
                <input name="contrasenia" type="password" id="inputPassword" class="control-input" placeholder="Escriba su contraseña" required>
                <label>Repetir Contraseña:</label>
                <input name="contrasenia" type="password" id="inputPassword" class="control-input" placeholder="Por favor, repita su contraseña" required>
                <label>Nombre:</label>
                <input name="nombre" type="text" class="control-input" placeholder="Escriba su nombre" required>
                <label>Apellidos:</label>
                <input name="apellidos" type="text" class="control-input" placeholder="Escriba sus apellidos" required>
                <button class="boton-class boton-entrar" type="submit">Registrarme</button>
            </form>
        </div>
    </div>
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/app/js/componentes.js" type="text/javascript"></script>
</body>
</html>