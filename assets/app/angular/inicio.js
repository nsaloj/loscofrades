var CofradesApp = angular.module("CofradesApp", [
    "ui.router", 
    "oc.lazyLoad",  
    "ngSanitize",
    "ngRoute",
    'ngSocket',
    'ngDialog',
    'ngAudio'
]); 

CofradesApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

CofradesApp.config(['$controllerProvider', function($controllerProvider) {
  $controllerProvider.allowGlobals();
}]);

CofradesApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

CofradesApp.run(['$rootScope','$location','Auth','wsFactory','$window','$state','webSocket', function($rootScope, $location,Auth,wsFactory,$window,$state,webSocket) {
    console.log('Inicio CofradesApp');
    
    $rootScope.msjSocket = function(Origen, Destino, Mensaje){
        var msj = {"Origen":Origen,"Destino":Destino,"Mensaje":Mensaje};
        webSocket.enviar(JSON.stringify(msj));
    }
    
    $rootScope.redirigir = function(ruta)
    {
        $location.path(ruta);
        $location.replace();
    }
    $rootScope.redirigir = function(ruta, param, valor)
    {
        $ubicacion = {};
        $ubicacion[param] = valor;
        $location.path(ruta).search($ubicacion);
        $location.replace();
    }
    
    $rootScope.secciones = [];
    var getSecciones = function(){
        wsFactory.getSecciones().then(function(response) {
            if(isEmpty(response.data.error))
            {
                $rootScope.secciones = response.data;
            }
            else console.log('Un error ocurrió durante la carga ');
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
        Auth.AuthAsync().then(function(user) {
                if (user === false && toState.authenticate)
                {
                    $window.location.assign("logout");
                }
                else
                {
                    if(!isEmpty(toState.rolId)){
                        if(($rootScope.enArray(parseInt(user.Rol_id),toState.rolId)) === true)
                        {
                            $rootScope.usuario = user;
                            getSecciones();
                        }
                        else{
                            if(parseInt(user.Rol_id) == 4 || parseInt(user.Rol_id) == 2)
                            {
                                $location.path('/comedor');
                            }
                            else if(parseInt(user.Rol_id) == 3) 
                            {
                                $location.path('/cocina');
                            }
                        }
                    }
                    else if(!(toState.url.indexOf('logout') > -1))
                    {
                        $rootScope.usuario = user;
                        getSecciones();
                    }
                }
            }
        );
     });
     $rootScope.enArray = function (value, array) {
      return (array.indexOf(value) > -1);
     }
}]);

/* Controlador principal de la aplicación */
CofradesApp.controller('AppController', ['$rootScope','$scope','$location',function ($rootScope, $scope,$location) {
    //Verificar que pestaña está activa
    $scope.esActivo = function(seccion)
    {
        
        var act = '';
        if($location.path().indexOf('informes/productos') > -1 && seccion.indexOf('productos') > -1)
        {
            act = '';
        }
        else
        {
            act = ($location.path().indexOf(seccion) > -1)? 'active' : (($location.path() === '' && seccion === '/comedor')?  'active':'');
        }
        return act;
    }
}]);

/*.run(["$rootScope", "$location", "Auth", function($rootScope, $location, Auth) {
    alert('verify');
    $rootScope.$on('$routeChangeStart', function (event) {
        
        if (!Auth.estaLogueado()) {
            console.log('DENY');
            event.preventDefault();
            $location.path('/cocina');
        }
        else {
            console.log('ALLOW');
            $location.path('/comedor');
        }
    });
}]);*/