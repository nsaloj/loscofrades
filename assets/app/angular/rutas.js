/* Routing para todas las páginas */
CofradesApp.config(['$stateProvider', '$urlRouterProvider',"$locationProvider", 
                    function($stateProvider, $urlRouterProvider ,$locationProvider, $location) {
    
    //$locationProvider.html5Mode(true);
    
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/comedor");  
    
    $stateProvider

        // Comedor
        .state('comedor', {        
            url:"/comedor",
            templateUrl: "assets/app/plantillas/comedor/comedor.html",
            data: {pageTitle: 'LosCofrades | Comedor'},
            controller: "ComedorController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/ComedorController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/comedor.css'                            
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,4]
        })
        .state('detalle', {
            parent: 'comedor',
            url: "/mesa/:id",
            templateUrl: "assets/app/plantillas/comedor/mesas/detalle-mesa.html",
            controller: "DetalleMesaController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/DetalleMesaController.js'
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,4]
        })
        .state('accion', {
            parent: 'comedor',
            url: "/nueva/mesa",
            templateUrl: "assets/app/plantillas/comedor/mesas/nueva-mesa.html",
            controller: "AccionMesaController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp', 
                        files: [
                            'assets/app/angular/controllers/AccionMesaController.js'
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,4]
        })
        .state('cocina', {        
            url: "/cocina",
            templateUrl: "assets/app/plantillas/cocina/cocina.html",
            data: {pageTitle: 'LosCofrades | Cocina'},
            controller: "CocinaController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/CocinaController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/cocina.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,3]
        })
        .state('productos', {
        
            url: "/productos?ruta",
            templateUrl: "assets/app/plantillas/productos/productos.html",
            data: {pageTitle: 'LosCofrades | Productos'},
            controller: "ProductosController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                          'assets/app/angular/controllers/ProductosController.js',
                          'assets/app/js/view-port.js',
                          'assets/app/css/productos.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,3,4]
        })
        .state('accionproducto', {
            parent: 'productos',
            url: "/accion/producto?id",
            templateUrl: "assets/app/plantillas/productos/accion-producto.html",
            controller: "AccionProductoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/AccionProductoController.js'
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,3,4]
        })
        .state('accioncategoria', {
            parent: 'productos',
            url: "/accion/categoria?id",
            templateUrl: "assets/app/plantillas/productos/categorias/accion-categoria.html",
            controller: "AccionProductoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/AccionProductoController.js'
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,3,4]
        })
        .state('suministros', {
        
            url: "/suministros",
            templateUrl: "assets/app/plantillas/suministros.html",
            data: {pageTitle: 'LosCofrades | Suministros'},
            controller: "SuministrosController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                          'assets/app/angular/controllers/GeneralController.js'  
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1,2,3]
        })
        .state('informes', {
            abstract: true,
            url: "/informes",
            templateUrl: "assets/app/plantillas/informes/informes.html",
            controller: "InformesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/InformesController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/productos.css' 
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('informes.ventas', {
            url: "/ventas",
            templateUrl: "assets/app/plantillas/informes/detalle/ventas.html",
            data: {pageTitle: 'LosCofrades | Informes'},
            controller: "InformesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                          'assets/app/angular/controllers/InformesController.js',
                          'assets/app/js/view-port.js',
                          'assets/app/css/productos.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('informes.ventasdia', {
            url: "/ventas/:fecha",
            templateUrl: "assets/app/plantillas/informes/detalle/ventas-fecha.html",
            data: {pageTitle: 'LosCofrades | Informes'},
            controller: "DetalleInformeController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                          'assets/app/angular/controllers/DetalleInformeController.js',
                          'assets/app/js/view-port.js',
                          'assets/app/css/productos.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('informes.venta', {
            url: "/venta/:id",
            templateUrl: "assets/app/plantillas/informes/detalle/ventas-detalle.html",
            data: {pageTitle: 'LosCofrades | Informes'},
            controller: "VentaController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                          'assets/app/angular/controllers/VentaController.js',
                          'assets/app/js/view-port.js',
                          'assets/app/css/productos.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('informes.productos', {
            url: "/productos",
            templateUrl: "assets/app/plantillas/informes/detalle/productos.html",
            data: {pageTitle: 'LosCofrades | Informes'},
            controller: "InformesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/InformesController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/productos.css' 
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('informes.afluencia', {
            url: "/afluencia",
            templateUrl: "assets/app/plantillas/informes/detalle/afluencia.html",
            data: {pageTitle: 'LosCofrades | Informes'},
            controller: "InformesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/InformesController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/productos.css' 
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('usuarios', {
        
            url: "/usuarios",
            templateUrl: "assets/app/plantillas/usuarios/usuarios.html",
            data: {pageTitle: 'LosCofrades | Usuarios'},
            controller: "UsuariosController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/UsuariosController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/productos.css' 
                        ] 
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('accionusuario', {
            parent: 'usuarios',
            url: "/accion/usuario?id",
            templateUrl: "assets/app/plantillas/usuarios/accion-usuario.html",
            controller: "AccionUsuarioController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/AccionUsuarioController.js'
                        ]
                    });
                }]
            },
            authenticate: true,
            rolId : [1]
        })
        .state('perfil', {
            url: "/perfil",
            templateUrl: "assets/app/plantillas/perfil.html",
            controller: "PerfilController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CofradesApp',
                        files: [
                            'assets/app/angular/controllers/PerfilController.js',
                            'assets/app/js/view-port.js',
                            'assets/app/css/perfil.css' 
                        ]
                    });
                }]
            },
            authenticate: true
        })
        .state('logout', {
            url: '/logout',
            controller: function($scope,$route,Auth,wsFactory,$window) {
                Auth.setUsuario(null);
                $window.location.assign("logout");
            },
            authenticate: true
        })
}]);