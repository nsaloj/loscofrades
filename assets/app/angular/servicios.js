CofradesApp.service('wsService', function($http) {

    var urlBase = 'api/v1/';
    
    this.getMesas = function () {
        return $http.post(urlBase + 'mesas/all');
    };
    this.getMesa = function (MesaId) {
        return $http.post(urlBase+"mesas/get/"+MesaId);
    };
});