angular.module('CofradesApp').controller('VentaController',['$rootScope','$scope','wsFactory','$location','$stateParams','ngDialog','$filter',function($rootScope, $scope, wsFactory,$location,$stateParams,ngDialog,$filter) {
    $scope.id = $stateParams.id;
    
    var getInforme = function(){
        $scope.Venta = [];
        wsFactory.getInformeVenta($scope.id).then(function(response) {
           if(isEmpty(response.data.error))
           {
               $scope.Venta = response.data;
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getInforme();
    
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.Venta.length; i++){
            total += parseFloat($scope.Venta[i].Total);
        }
        return total;
    }
}]);