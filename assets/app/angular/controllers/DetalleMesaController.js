angular.module('CofradesApp').controller('DetalleMesaController', ['wsFactory','$rootScope','$scope','$stateParams','webSocket','$location','ngDialog',function(wsFactory, $rootScope, $scope, $stateParams,webSocket,$location,ngDialog) {
    
    var MesaId = $stateParams.id    
    $scope.origen = 'pagina-pedidos';
    $scope.data_cantidad = 1;
    $scope.data_comensales = 1;
    $scope.pedido;
    $scope.Mesa;
    $scope.productos;
    var Pedido_id = 1;
    var pedidos_bd;
    
    $scope.cambiarComensales = function(valor)
    {
        $scope.data_comensales = valor;
        if($scope.pedido)
        {
            $scope.pedido.Comensales = valor;
        }
    }
    var getMesa = function()
    {
        wsFactory.getMesa(MesaId).then(function(response) {
            if(isEmpty(response.data.error)) 
            {
                $scope.Mesa = response.data;
                if($scope.Mesa.Comensales !== null)  $scope.data_comensales = parseInt($scope.Mesa.Comensales);
            }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getMesa();
    
    $rootScope.$watch('reloadDetalleMesa', function(reloadComedor) {
         getPedido();
    });
    
    var getPedido = function()
    {
        wsFactory.getPedidoMesa(MesaId).then(function(response) {
            if($scope.pedido)
            {
                $scope.pedido.Pedidos = [];
                $scope.pedido = null;            
            }
            if(isEmpty(response.data.error)) 
            {
                $scope.pedido = response.data;
                pedidos_bd = [];
                pedidos_bd.push.apply(pedidos_bd, response.data.Pedidos);
            }
            else{
                console.log('Un error ocurrió durante la carga ');
            }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getPedido();
    
    wsFactory.getProductos().then(function(response) {
       if(isEmpty(response.data.error)) $scope.productos = response.data;
    }, function(error) {
        console.log('Un error ocurrió durante la carga ' + error);
    });
    
        
    /*Funciones locales*/
    
    $scope.entregados;
    $scope.$watch('pedido', function(newVal, oldVal){
        $scope.entregados = entregados();
    }, true);
    var entregados = function(){
        var valor = 0;
        if($scope.pedido)
        {
            var comArr = eval( $scope.pedido.Pedidos );
            if(comArr)
            {                
                for( var i = 0; i < comArr.length; i++ ) {
                    var elem = comArr[i];
                    
                    if(parseInt(elem['EstadoDetalle_id']) === 4)
                    {
                        valor = valor+1;
                    }
                }
            }
        }
        return valor;
    }
    
    
    var existe = function(id, valor){
        var comArr = eval( $scope.pedido.Pedidos );
        if(comArr)
        {
            for( var i = 0; i < comArr.length; i++ ) {
                var elem = comArr[i];
                if(elem[id] === valor)
                {
                    return elem;
                }
            }
        }
        return false;
    }
    var existeBD = function(id, valor){
        var comArr = eval( pedidos_bd );
        if(comArr)
        {
            for( var i = 0; i < comArr.length; i++ ) {
                var elem = comArr[i];
                if(elem[id] === valor)
                {
                    return elem;
                }
            }
        }
        return false;
    }
    
    var actualizarPedido = function(Producto_id, campos, valores){
        var comArr = eval( $scope.pedido.Pedidos );
        if(comArr)
        {
            for( var i = 0; i < comArr.length; i++ ) {
                var elem = comArr[i];
                if(elem['Producto_id'] === Producto_id)
                {
                    var obj = comArr[i];
                    //obj['campo'] = valores;
                    for( var j = 0; j < campos.length; j++ ) {
                        obj[campos[j]] = valores[j];
                    }
                    break;
                }
            }
        }
    } 
    
    /*Funciones angular*/
    $scope.quitarPedido = function(id){
        var index = -1;
        var comArr = eval( $scope.pedido.Pedidos );
        for( var i = 0; i < comArr.length; i++ ) {
            if( comArr[i].Detalle_id === id ) {
                if(existeBD('Detalle_id', comArr[i].Detalle_id) !== false)
                {
                    comArr[i].EstadoDetalle_id = -1;
                }
                else{
                    $scope.pedido.Pedidos.splice( i, 1 );
                }
                break;
            }
        }        
    }
    
    $scope.agregarPedido = function()
    {
        
        if(!$scope.pedido)
        {
            $scope.pedido = {'Comensales':$scope.data_comensales, 'Mesa_id':MesaId,'Usuario_id':$rootScope.usuario.Usuario_id,'EstadoPedido_id':1,'Pedidos':new Array(0)};
            //{'Detalle_id':null,'Producto_id':null,'Producto': null,'Producto_precio': null, 'Cantidad':null,'EstadoDetalle_id':null}
        }
        if(!isEmpty($scope.data_producto) && ($scope.data_cantidad))
        {
            var producto_id = $scope.data_producto.Producto_id;
            var elemento = existe(producto_id,'Producto_id');
            /*if(elemento === false)
            {*/
                $scope.pedido.Pedidos.push({'Detalle_id':Pedido_id+'a','Producto_id': $scope.data_producto.Producto_id,'Producto': $scope.data_producto.Nombre,'Producto_precio': $scope.data_producto.Precio, 'Cantidad':$scope.data_cantidad,'EstadoDetalle_id':1});
                Pedido_id = Pedido_id+1;
                $scope.data_producto = [];
                $scope.data_cantidad = 1;
            /*}
            else
            {
                var cantidad_ = elemento.Cantidad+$scope.data_cantidad;
                elemento.Cantidad = cantidad_;
                $scope.data_producto = [];
                $scope.data_cantidad = 1;
            }*/
        }
    }
    
    
       
    $scope.actualizarPedido = function(producto_id, campo, valor)
    {
        var elemento = existe(producto_id,'Producto_id');
        if(elemento !== false)
        {
            actualizarPedido(producto_id, [campo],[valor]);
        }        
    }
    
    $scope.confirmarPedido = function()
    {
        if($scope.pedido)
        {
            wsFactory.crearPedido(JSON.stringify($scope.pedido)).then(function(data) {
                if(data.data.data === true)
                {                    
                    $rootScope.msjSocket($scope.origen, ['pagina-cocina','pagina-bebidas'],'nuevo-pedido');
                    $rootScope.msjSocket($scope.origen, ['pagina-comedor'],'cargar-mesas');
                    $rootScope.reloadComedor = !$rootScope.reloadComedor;
                    $rootScope.reloadBebidas = !$rootScope.reloadBebidas;
                    $rootScope.redirigir('/comedor');
                }
            });
        }
        //else console.log("no hay");
    }
    
    $scope.setProducto = function(data)
    {
        $scope.data_producto = data;
    }
    
        
        
    /*Dialog*/
    $scope.comentar = function (modelo) {
        //ngDialog.open({ template: 'assets/app/angular/componentes/dialog-comment.html', className: 'ngdialog-theme-default' });
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-comment.html',
            className: 'ngdialog-theme-plain',
            showClose: false,
            controller: ['$scope', function(scope) {
                scope.pedido = $.extend( {}, modelo);
            }]
        }).then(function (value) {
           modelo.Comentario = value;
        }, function (reason) {
            
        });
    };
    
    $scope.cambiarMesa = function () {
        //ngDialog.open({ template: 'assets/app/angular/componentes/dialog-comment.html', className: 'ngdialog-theme-default' });
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-cambiarMesa.html',
            className: 'ngdialog-theme-plain',
            disableAnimation: true,
            showClose: false,
            controller: ['$scope','wsFactory', function(scope,wsFactory) {
                scope.Mesa = $.extend( {}, $scope.Mesa);
                scope.Mesa_ = 0;
                scope.Mesas;
                wsFactory.getMesas().then(function(response) {
                    if(isEmpty(response.data.error)){
                        scope.Mesas = response.data;
                    }
                    else console.log('Un error ocurrió durante la carga ' + error);
                }, function(error) {
                    console.log('Un error ocurrió durante la carga ' + error);
                });
            }]
        }).then(function (value) {
            if($scope.pedido && parseInt(value) > 0)
            {
               var pedido = $scope.pedido.Pedido_id;
               wsFactory.cambiarMesa(pedido,value).then(function(data) {
                    if(data.data)
                    {
                        $rootScope.reloadComedor = !$rootScope.reloadComedor;
                        $scope.redirigir('/comedor/mesa/'+value);
                    }
                });
            }
            else{
                alert('No se seleccionó ninguna mesa');
            }
        }, function (reason) {
            
        });
    };
    
    $scope.buscarProducto = function () {
        //ngDialog.open({ template: 'assets/app/angular/componentes/dialog-comment.html', className: 'ngdialog-theme-default' });
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-buscarProducto.html',
            appendClassName: 'dialog-full',
            width: '90%',
            showClose: false,
            disableAnimation: true,
            controller: ['$scope','wsFactory', function(scope,wsFactory) {
                scope.productos = $scope.productos;
                scope.categorias = [];
                scope.listaProductos = [];
                scope.productosCategoria;
                
                $scope.total;
                $scope.itemXpag = 15;
                $scope.pagina = 1;
                $scope.paginas = 0;
                $scope.inicio = ($scope.pagina-1)*parseInt($scope.itemXpag);
                
                
                var rehacer = function(){
                    $scope.total = scope.productosCategoria.length;
                    $scope.paginas = Math.ceil($scope.total/$scope.itemXpag);
                    $scope.inicio = ($scope.pagina-1)*parseInt($scope.itemXpag);
                }
                
                wsFactory.getArbolCategorias().then(function(response) {
                   if(isEmpty(response.data.error))
                    {
                        scope.categorias = response.data;
                    }
                    else{
                        console.log("error");
                    }
                }, function(error) {
                    console.log('Un error ocurrió durante la carga ' + error);
                });
                scope.buscarCategoria = function(id){
                    scope.productosCategoria = [];
                    wsFactory.getproductosCategoria(id).then(function(response) {
                       if(isEmpty(response.data.error))
                        {
                            scope.productosCategoria = response.data;
                            rehacer();
                        }
                        else{
                            console.log("error");
                        }
                    }, function(error) {
                        console.log('Un error ocurrió durante la carga ' + error);
                    });
                }
                var PedidoD_id = 0;
                scope.agregarProducto = function(producto, cantidad, event){
                    if(event.target.nodeName === 'TD'){
                        if(!scope.existe(producto.Producto_id))
                        {
                            scope.listaProductos.push({'Detalle_id':PedidoD_id+"b",'Producto_id': producto.Producto_id,'Producto': producto.Nombre,'Producto_precio': producto.Precio, 'Cantidad':cantidad,'EstadoDetalle_id':1});
                            PedidoD_id = PedidoD_id+1;
                        }
                        else{

                            for(var i = 0; i < scope.listaProductos.length; i++)
                            {
                                 var elem = scope.listaProductos[i];
                                if(elem.Producto_id === producto.Producto_id)
                                {
                                    elem.Cantidad += cantidad;
                                    break;
                                }
                            }
                        }
                    }
                }
                scope.quitarProducto = function(id){
                    if(scope.listaProductos)
                    {
                        for(var i = 0; i < scope.listaProductos.length; i++)
                        {
                            var elem = scope.listaProductos[i];
                            if(elem.Producto_id === id)
                            {
                                scope.listaProductos.splice( i, 1 );
                                break;
                            }
                        }
                    }
                    return false;
                }
                scope.existe = function(id){
                    if(scope.listaProductos)
                    {
                        for(var i = 0; i < scope.listaProductos.length; i++)
                        {
                            var elem = scope.listaProductos[i];
                            if(elem.Producto_id === id)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                scope.productoAgregado = function(id){
                    if(scope.listaProductos)
                    {
                        for(var i = 0; i < scope.listaProductos.length; i++)
                        {
                            var elem = scope.listaProductos[i];
                            if(elem['Producto_id'] === id)
                            {
                                return scope.listaProductos[i];
                            }
                        }
                    }
                    return false;
                }                
    
            }]
        }).then(function (value) {
            if(value.length > 0)
            { 
                if(!$scope.pedido)
                {
                    $scope.pedido = {'Comensales':$scope.data_comensales, 'Mesa_id':MesaId,'Usuario_id':$rootScope.usuario.Usuario_id,'EstadoPedido_id':1,'Pedidos':[]};
                }
                $scope.pedido.Pedidos.push.apply($scope.pedido.Pedidos, value);
            }
        }, function (reason) {
            
        });
    };
    
    
    
    
    
    
    
    $scope.cobroParcial = function () {
        //ngDialog.open({ template: 'assets/app/angular/componentes/dialog-comment.html', className: 'ngdialog-theme-default' });
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-pagoParcial.html',
            appendClassName: 'dialog-full',
            width: '90%',
            showClose: false,
            disableAnimation: true,
            controller: ['$scope','wsFactory', function(scope,wsFactory) {
                scope.pedidos = [];
                scope.listaDetalles = [];
                scope.TOTAL= false;
                
                var getTotalConsumido = function(){
                    var consumido = 0;
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {                
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                consumido = consumido+(parseFloat(elem['Producto_precio'])*parseInt(elem['Cantidad']));
                            }
                        }
                    }
                    scope.TotalConsumido = consumido;
                }
                var getTotalPagado = function(){
                    var pagado = 0;
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {                
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                pagado = pagado+(parseFloat(elem['Producto_precio'])*  (  parseInt(elem['Cantidad'])-parseInt(elem['noPagados'])    )  );
                            }
                        }
                    }
                    scope.TotalPagado = pagado;
                }
                var getPedido = function()
                {
                    wsFactory.getEntregadoMesa(MesaId).then(function(response) {
                        if(isEmpty(response.data.error)) 
                        {
                            scope.pedidos = response.data;
                            getTotalPagado();
                            getTotalConsumido();
                        }
                        else{
                            console.log('Un error ocurrió durante la carga ');
                        }
                    }, function(error) {
                        console.log('Un error ocurrió durante la carga ' + error);
                    });
                }
                getPedido();
                scope.agregarDetalle = function(detalle, cantidad, event){
                    if(detalle.noPagados > 0)
                    {
                        if(event.target.nodeName === 'TD'){
                            if(!scope.existe(detalle.Detalle_id))
                            {
                                scope.listaDetalles.push({'Detalle_id':detalle.Detalle_id,'Cantidad':cantidad});
                            }
                            else{
                                for(var i = 0; i < scope.listaDetalles.length; i++)
                                {
                                    var elem = scope.listaDetalles[i];
                                    if(elem.Detalle_id === detalle.Detalle_id)
                                    {
                                        if(elem.Cantidad < detalle.noPagados)
                                        {
                                            elem.Cantidad += cantidad;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                
                scope.quitarDetalle = function(id){
                    if(scope.listaDetalles)
                    {
                        for(var i = 0; i < scope.listaDetalles.length; i++)
                        {
                            var elem = scope.listaDetalles[i];
                            if(elem.Detalle_id === id)
                            {
                                scope.listaDetalles.splice( i, 1 );
                                break;
                            }
                        }
                    }
                    return false;
                }
                scope.existe = function(id){
                    if(scope.listaDetalles)
                    {
                        for(var i = 0; i < scope.listaDetalles.length; i++)
                        {
                            var elem = scope.listaDetalles[i];
                            if(elem.Detalle_id === id)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }
                scope.getTOTAL= function(){
                    var TOTAL = 0;
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                var elemDetalle = scope.detalleAgregado(elem.Detalle_id);
                                if(elemDetalle)
                                {
                                    var tot = parseInt(elemDetalle['Cantidad'])*parseFloat(elem['Producto_precio']);
                                    TOTAL = TOTAL + tot;
                                }
                            }
                        }
                    }
                    //console.log("busca "+TOTAL+"  --  "+(scope.TotalConsumido - scope.TotalPagado));
                    if((TOTAL === (scope.TotalConsumido - scope.TotalPagado)))
                    {
                        return "si";
                    }
                    return "no";
                }
                scope.detalleAgregado = function(id){
                    if(scope.listaDetalles)
                    {
                        for(var i = 0; i < scope.listaDetalles.length; i++)
                        {
                            var elem = scope.listaDetalles[i];
                            if(elem.Detalle_id === id)
                            {                                
                                return elem;
                            }
                        }
                    }
                    return false;
                }
            
            }]
        }).then(function (value) {
            if(value)
            {        
                //console.log(value);
                var pedidoPago = {'Pedido_id':$scope.pedido.Pedido_id,'Pedidos':value.lista,'TOTAL':value.TOTAL};
                wsFactory.pagoParcial(JSON.stringify(pedidoPago)).then(function(data) {
                    if(data.data.data === true)
                    {                    
                        $rootScope.msjSocket($scope.origen, ['pagina-pedidos','cargar-mesas'],'pedido-cobrado');
                        $rootScope.reloadDetalleMesa = !$rootScope.reloadDetalleMesa;
                        $rootScope.reloadComedor = !$rootScope.reloadComedor;
                        if(value.TOTAL === 'si')
                        {                            
                            $rootScope.redirigir('/comedor');
                        }
                    }
                });
            }
        }, function (reason) {
            
        });
    };
    
    
    
    
    
    
    
    
    
    $scope.cobroTotal = function () {
        //ngDialog.open({ template: 'assets/app/angular/componentes/dialog-comment.html', className: 'ngdialog-theme-default' });
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-pagoTotal.html',
            appendClassName: 'dialog-full',
            width: '90%',
            showClose: false,
            disableAnimation: true,
            controller: ['$scope','wsFactory', function(scope,wsFactory) {
                scope.pedidos = [];
                scope.listaDetalles = [];
                
                var getTotalConsumido = function(){
                    var consumido = 0;
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {                
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                consumido = consumido+(parseFloat(elem['Producto_precio'])*parseInt(elem['Cantidad']));
                            }
                        }
                    }
                    scope.TotalConsumido = consumido;
                }
                var getTotalPagado = function(){
                    var pagado = 0;
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {                
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                pagado = pagado+(parseFloat(elem['Producto_precio'])*  (  parseInt(elem['Cantidad'])-parseInt(elem['noPagados'])    )  );
                            }
                        }
                    }
                    scope.TotalPagado = pagado;
                }
                var getPedido = function()
                {
                    wsFactory.getEntregadoMesa(MesaId).then(function(response) {
                        if(isEmpty(response.data.error)) 
                        {
                            scope.pedidos = response.data;
                            getTotalPagado();
                            getTotalConsumido();
                            llenarDetalles();
                        }
                        else{
                            console.log('Un error ocurrió durante la carga ');
                        }
                    }, function(error) {
                        console.log('Un error ocurrió durante la carga ' + error);
                    });
                }
                getPedido();
                
                var llenarDetalles = function(){
                    scope.listaDetalles= [];
                    if(scope.pedidos)
                    {
                        var comArr = eval( scope.pedidos.Pedidos );
                        if(comArr)
                        {
                            for( var i = 0; i < comArr.length; i++ ) {
                                var elem = comArr[i];
                                var cantidad = parseInt(elem['Cantidad'])-(  parseInt(elem['Cantidad'])-parseInt(elem['noPagados'])    );
                                
                                if(cantidad > 0)
                                {
                                    scope.listaDetalles.push({'Detalle_id':elem.Detalle_id,
                                                              'Cantidad':cantidad});
                                }
                            }
                        }
                    }   
                }            
            }]
        }).then(function (value) {
            if(value)
            {        
                var pedidoPago = {'Pedido_id':$scope.pedido.Pedido_id,'Pedidos':value,'TOTAL':'si'};
                wsFactory.pagoParcial(JSON.stringify(pedidoPago)).then(function(data) {
                    if(data.data.data === true)
                    {                    
                        $rootScope.msjSocket($scope.origen, ['pagina-pedidos','cargar-mesas'],'pedido-cobrado');
                        $rootScope.reloadDetalleMesa = !$rootScope.reloadDetalleMesa;
                        $rootScope.reloadComedor = !$rootScope.reloadComedor;
                        $rootScope.redirigir('/comedor');
                    }
                });
            }
        }, function (reason) {
            
        });
    };
    /*Fin dialogs*/
        
        
        
    /*Acciones de mesa*/
     $scope.eliminarMesa = function(){
        if(!isEmpty(MesaId))
        {
             wsFactory.eliminarMesa(MesaId).then(function(response) {
                if(isEmpty(response.data.error))
                {
                    $rootScope.reloadComedor = !$rootScope.reloadComedor;
                    $scope.redirigir('/comedor');
                }
            }, function(error) {
                console.log('Un error ocurrió durante la eliminación ' + error);
            });
        }
    }
    
    var ws = webSocket.getWS();
    ws.onMessage(function (msg) {
        if(msg)
        {
            var datos = JSON.parse(JSON.parse(msg.data).message);
            if(datos){
                if(datos.Destino.indexOf($scope.origen) > -1)
                {
                    getMesa();
                    getPedido();
                }
            }
        }
    });
}]);