angular.module('CofradesApp').controller('AccionProductoController', ['wsFactory','$rootScope','$scope','$location','$window',"$stateParams",function(wsFactory, $rootScope, $scope, $location,$window,$stateParams) {
    $scope.id = $stateParams.id;
    $scope.Categoria;
    $scope.Producto;
    $scope.Categorias_top;
    $scope.Categorias;    
    $scope.Categoria_id = 0;
    
    var getObjeto = function(valor, id){
        if(valor === 1){
            wsFactory.getProducto(id).then(function(response) {
               if(isEmpty(response.data.error))
               {
                   $scope.Producto = response.data;
                   $scope.Nombre = $scope.Producto.Nombre;
                   $scope.Precio = $scope.Producto.Precio;
                   $scope.Categoria_id = $scope.Producto.Categoria_id;
               }
               else console.log('Un error ocurrió durante la carga ' + error);
            }, function(error) {
                console.log('Un error ocurrió durante la carga ' + error);
            });
        }
        else{
            wsFactory.getCategoria(id).then(function(response) {
               if(isEmpty(response.data.error))
               {
                   $scope.Categoria = response.data;
                   $scope.Padre_id = parseInt($scope.Categoria.Padre_id);
                   $scope.NombreCategoria = $scope.Categoria.Nombre;
               }
               else console.log('Un error ocurrió durante la carga ' + error);
            }, function(error) {
                console.log('Un error ocurrió durante la carga ' + error);
            });
        }
    }
    if($scope.id){
        if($location.path().indexOf('accion/categoria') > -1)
        {
            getObjeto(2, $scope.id);
        }
        else if($location.path().indexOf('accion/producto') > -1)
        {
            getObjeto(1, $scope.id);
        }
    }
    
    
        
    wsFactory.getCategoriasHijos().then(function(response) {
       if(isEmpty(response.data.error)) $scope.Categorias = response.data;
    }, function(error) {
        console.log('Un error ocurrió durante la carga ' + error);
    });
    
        
    var element = $('.side-box');
    $(element).on("click", function(event){
        if(event.target.className.indexOf('side-box') > -1)
        {
            $scope.$apply(function () {
                $scope.cerrar();
            });
        }
    });
    
    $scope.guardar = function(){
        
        if(!isEmpty($scope.Nombre) && $scope.Precio > 0 && $scope.Categoria_id > 0)
        {
            var datos = {'Nombre':$scope.Nombre, 'Precio':$scope.Precio, 'Categoria_id':$scope.Categoria_id,'Producto_id':$scope.id};
             wsFactory.crearProducto(datos).then(function(data) {
                if(data.data.data === 'true' || data.data.data === true)
                {
                    $rootScope.reloadProductos = !$rootScope.reloadProductos;
                    $rootScope.redirigir('/productos');
                }
                else alert('Hubo un error, por favor, intentelo de nuevo')
            });            
        }
        else alert('Por favor llene todos los campos');
    }
    
    $scope.cerrar = function()
    {
        if($location.path().indexOf('accion/categoria') > -1)
        {
            $rootScope.reloadComedor = !$rootScope.reloadComedor;
            $rootScope.redirigir('/productos','ruta','categorias');
        }
        else if($location.path().indexOf('accion/producto') > -1)
        {
            $rootScope.reloadComedor = !$rootScope.reloadComedor;
            $rootScope.redirigir('/productos');
        }        
    }
    //var element = $('.sidenav').css({display:'none'});
    //$window.onbeforeunload =  $scope.onExit;
    
    
    //CATEGORIAS    
    
    wsFactory.getCategoriasTop().then(function(response) {
       if(isEmpty(response.data.error)) $scope.Categorias_top = response.data;
    }, function(error) {
        console.log('Un error ocurrió durante la carga ' + error);
    });
    $scope.guardarCategoria = function(nombre, padre){
        
        if(!isEmpty(nombre) && padre > 0)
        {
            var datos = {'Nombre':nombre, 'Padre_id':padre, 'Categoria_id':$scope.id};
             wsFactory.crearCategoria(datos).then(function(data) {
                if(data.data.data === 'true' || data.data.data === true)
                {
                    $rootScope.reloadCategorias = !$rootScope.reloadCategorias;
                    $rootScope.redirigir('/productos','ruta','categorias');
                }
                else alert('Hubo un error, por favor, intentelo de nuevo')
            });            
        }
        else alert('Por favor llene todos los campos');
    }
}]);
