angular.module('CofradesApp').controller('AccionMesaController', ['wsFactory','$rootScope','$scope','$location','$window',function(wsFactory, $rootScope, $scope, $location,$window) {
    $scope.Mesas;
        
    var element = $('.side-box');
    $(element).on("click", function(event){
        if(event.target.className.indexOf('side-box') > -1)
        {
            $scope.$apply(function () {
                $scope.cerrar();
            });
        }
    });
    
    $scope.guardar = function(){
        
        if(!isEmpty($scope.Nombre))
        {
            var datos = {'Nombre':$scope.Nombre};
             wsFactory.crearMesa(datos).then(function(data) {
                if(data.data)
                {
                    $rootScope.reloadComedor = !$rootScope.reloadComedor;
                    $rootScope.msjSocket($scope.origen, ['cargar-mesas'],'nueva-mesa');
                    $rootScope.redirigir('/comedor');
                }
            });            
        }
    }
    
    $scope.cerrar = function()
    {
        $rootScope.reloadComedor = !$rootScope.reloadComedor;
        $rootScope.redirigir('/comedor');
    }
    //var element = $('.sidenav').css({display:'none'});
    //$window.onbeforeunload =  $scope.onExit;
}]);