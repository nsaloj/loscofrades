angular.module('CofradesApp').controller('DetalleInformeController',['$rootScope','$scope','wsFactory','$location','$stateParams','ngDialog','$filter','$state',function($rootScope, $scope, wsFactory,$location,$stateParams,ngDialog,$filter,$state) {
    $scope.fecha = $stateParams.fecha;

    var toDate = function(dateStr) {
        var parts = dateStr.split("-");
        return new Date(parts[2], parts[1] - 1, parts[0]);
    }
    var getInforme = function(){
        $scope.ventas = [];
        var fecha = toDate($scope.fecha);
        wsFactory.getInformeVentasFecha($filter('date')(fecha, "yyyy-MM-dd")).then(function(response) {
           if(isEmpty(response.data.error))
           {
               $scope.ventas = response.data;               
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getInforme();
    
    $scope.vetDetalle = function(id, param){
        $state.go('informes.venta', { id: param });
    }
    
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.ventas.length; i++){
            total += parseFloat($scope.ventas[i].TOTAL);
        }
        return total;
    }
}]);