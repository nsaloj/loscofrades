angular.module('CofradesApp').controller('AccionUsuarioController', ['wsFactory','$rootScope','$scope','$location','$window',"$stateParams",function(wsFactory, $rootScope, $scope, $location,$window,$stateParams) {
    $scope.id = $stateParams.id;
    $scope.Usuario_;
    $scope.Usuario;
    $scope.Nombre;
    $scope.Apellidos;
     
    $scope.Rol_id = 4;
    $scope.Roles;
    
    var aleatorio = function(tam)
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkl0123456789mnopqrstuvwxyz";

        for( var i=0; i < tam; i++ )
        {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }
    $scope.Contrasenia = aleatorio(6);   
    
    
    
    var getUsuario = function(){
        if(!isEmpty($scope.id))
        {
            wsFactory.getUsuarioWS($scope.id).then(function(response) {
               if(isEmpty(response.data.error))
               {
                   $scope.Usuario_ = response.data;
                   $scope.Usuario = $scope.Usuario_.Usuario;
                   $scope.Nombre = $scope.Usuario_.Nombre;
                   $scope.Apellidos = $scope.Usuario_.Apellidos;
                   $scope.Rol_id = $scope.Usuario_.Rol_id;
               }
               else console.log('Un error ocurrió durante la carga ' + error);
            }, function(error) {
                console.log('Un error ocurrió durante la carga ' + error);
            });
        }
    }
    getUsuario();
    
    var getRoles = function()
    {
        wsFactory.getRoles().then(function(response) {
           if(isEmpty(response.data.error))
           {
               $scope.Roles = response.data;
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getRoles();
    
    var element = $('.side-box');
    $(element).on("click", function(event){
        if(event.target.className.indexOf('side-box') > -1)
        {
            $scope.$apply(function () {
                $scope.cerrar();
            });
        }
    });
    
    $scope.guardar = function(){
        
        if(!isEmpty($scope.Nombre) && !isEmpty($scope.Apellidos) && !isEmpty($scope.Rol_id) && !isEmpty($scope.Usuario))
        {
            var datos = {'Usuario_id':$scope.id,'Usuario':$scope.Usuario,'Nombre':$scope.Nombre,'Apellidos':$scope.Apellidos, 'Rol_id':$scope.Rol_id,'Contrasenia':$scope.Contrasenia};
             wsFactory.crearUsuario(datos).then(function(data) {
                if(data.data.data === 'true' || data.data.data === true)
                {
                    $rootScope.reloadUsuarios = !$rootScope.reloadUsuarios;
                    $rootScope.redirigir('/usuarios');
                }
                else alert('Hubo un error, por favor, intentelo de nuevo')
            });            
        }
        else alert('Por favor llene todos los campos');
    }
    
    $scope.cerrar = function()
    {
        $rootScope.reloadUsuarios = !$rootScope.reloadUsuarios;
        $rootScope.redirigir('/usuarios');
    }    
}]);
