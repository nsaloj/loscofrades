angular.module('CofradesApp').controller('InformesController',['$rootScope','$scope','wsFactory','$location','$stateParams','ngDialog','$filter','$state',function($rootScope, $scope, wsFactory,$location,$stateParams,ngDialog,$filter,$state) {
    $scope.ruta = $stateParams.ruta;
    $scope.ventas;
    $scope.productos;
    $scope.afluencias;
    
    
    $scope.esActivo = function(seccion)
    {
        var act = ($location.path().indexOf(seccion) > -1)? 'actual' : '';
        return act;
    }
    
    //$scope.inicio; 
    var date = new Date();
    $scope.inicio= date;
    $scope.final = date;
    
    $scope.filtro_direccion = 0;
    
    $scope.productos;    
    $scope.total;
    $scope.itemXpag = 15;
    $scope.pagina = 1;
    $scope.paginas = 0;
    $scope.filtro_categoria = 0;
    $scope.filtro_nombre = "";
    $scope.filtro_precio = 0;
    $scope.filtros = [];
    
    var getTotalVendido = function(){
        wsFactory.getTotalVendido().then(function(response) {
           if(isEmpty(response.data.error))
           {
               $scope.totalFecha = parseFloat(response.data.TOTAL);
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getTotalVendido();
    
    $scope.setFiltros = function()
    {
        $scope.filtros = [];
        
        if($scope.filtro_nombre)
        {            
            $scope.filtros = [{"campo":"Nombre","valor":$scope.filtro_nombre}];
        }
        if($scope.filtro_precio)
        {
            if($scope.filtros.length > 0) $scope.filtros.push({"campo":"Precio","valor":$scope.filtro_precio});
            else $scope.filtros =[{"campo":"Precio","valor":$scope.filtro_precio}];
        }
        if($scope.filtro_categoria)
        {
            if($scope.filtro_categoria > 0)
            {
                if($scope.filtros.length > 0) $scope.filtros.push({"campo":"Categoria","valor":$scope.filtro_categoria});
                else $scope.filtros =[{"campo":"Categoria","valor":$scope.filtro_categoria}];
            }
        }
        //cargarProductos();
    }
    $scope.getInformeVentas = function(){
        $scope.ventas = [];
        $scope.totalVendido = "";
        wsFactory.getInformeVentas({"fecha_inicio":$filter('date')($scope.inicio, "yyyy-MM-dd"),"fecha_fin":$filter('date')($scope.final, "yyyy-MM-dd"),"Dir":($scope.filtro_direccion)? (($scope.filtro_direccion == 0)? 'DESC':'ASC'):'DESC'}).then(function(response) {
            
           if(isEmpty(response.data.error))
           {
               $scope.ventas = response.data;      
               $scope.totalVendido = getTotal();
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    $scope.getInformeProductos = function(){
        $scope.productos = [];
        wsFactory.getInformeProductos({"fecha_inicio":$filter('date')($scope.inicio, "yyyy-MM-dd"),"fecha_fin":$filter('date')($scope.final, "yyyy-MM-dd"),"Dir":($scope.filtro_direccion)? (($scope.filtro_direccion == 0)? 'DESC':'ASC'):'DESC'}).then(function(response) {
            
           if(isEmpty(response.data.error))
           {
               $scope.productos = response.data;               
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    $scope.getInformeAfluencias = function(){
        $scope.afluencias = [];
        wsFactory.getInformeAfluencias({"fecha_inicio":$filter('date')($scope.inicio, "yyyy-MM-dd"),"fecha_fin":$filter('date')($scope.final, "yyyy-MM-dd"),"Dir":($scope.filtro_direccion)? (($scope.filtro_direccion == 0)? 'DESC':'ASC'):'DESC'}).then(function(response) {
            
           if(isEmpty(response.data.error))
           {
               $scope.afluencias = response.data;               
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    
    
    $rootScope.$watch('reloadProductos', function(reloadComedor) {
         //cargarProductos();
    }); 
    $scope.$watch('orderByItem+reverse', function(reloadComedor) {
        // cargarProductos();
    }); 
    
    
    $scope.getMostrar = function()
    {
        var act = ($location.path().indexOf('accion/') > -1);
        return act;
    }
    
    $scope.vetDetalle = function(id, param){
        $state.go('informes.ventasdia', { fecha: param });
    }
    $scope.getDia = function(dia){
        if(dia.toLowerCase().indexOf("monday") > -1) return "Lunes";
        else if(dia.toLowerCase().indexOf("tuesday") > -1) return "Martes";
        else if(dia.toLowerCase().indexOf("wednesday") > -1) return "Miércoles";
        else if(dia.toLowerCase().indexOf("Thursday") > -1) return "Jueves";
        else if(dia.toLowerCase().indexOf("friday") > -1) return "Viernes";
        else if(dia.toLowerCase().indexOf("saturday") > -1) return "Sábado";
        else if(dia.toLowerCase().indexOf("sunday") > -1) return "Domingo";
        else return dia;
    }
    
    var getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.ventas.length; i++){
            total += parseFloat($scope.ventas[i].TOTAL);
        }
        return total;
    }
}]);