angular.module('CofradesApp').controller('ProductosController',['$rootScope','$scope','wsFactory','$location','$stateParams','ngDialog', function($rootScope, $scope, wsFactory,$location,$stateParams,ngDialog) {
    $scope.ruta = $stateParams.ruta;
    $scope.productos;
    
    $scope.total;
    $scope.itemXpag = 15;
    $scope.pagina = 1;
    $scope.paginas = 0;
    $scope.filtro_categoria = 0;
    $scope.filtro_nombre = "";
    $scope.filtro_precio = 0;
    $scope.filtros = [];
    $scope.$watch('pagina', function(nuevo, anterior) {
        cargarProductos();
    });
        
    $scope.setFiltros = function()
    {
        $scope.filtros = [];
        
        if($scope.filtro_nombre)
        {            
            $scope.filtros = [{"campo":"Nombre","valor":$scope.filtro_nombre}];
        }
        if($scope.filtro_precio)
        {
            if($scope.filtros.length > 0) $scope.filtros.push({"campo":"Precio","valor":$scope.filtro_precio});
            else $scope.filtros =[{"campo":"Precio","valor":$scope.filtro_precio}];
        }
        if($scope.filtro_categoria)
        {
            if($scope.filtro_categoria > 0)
            {
                if($scope.filtros.length > 0) $scope.filtros.push({"campo":"Categoria","valor":$scope.filtro_categoria});
                else $scope.filtros =[{"campo":"Categoria","valor":$scope.filtro_categoria}];
            }
        }
        cargarProductos();
    }
    var cargarProductos = function(){
        
        var inicio = ($scope.pagina-1)*parseInt($scope.itemXpag);
        var orderBy = $scope.orderByitem+" "+(($scope.reverse === true)? "DESC":"ASC");
        var data = {"inicio":inicio,"cantidad":$scope.itemXpag}; wsFactory.getProductosFull({"inicio":inicio,"cantidad":$scope.itemXpag,"orderBy":orderBy,"filterBy":$scope.filtros}).then(function(response) {
           $scope.productos = [];
           if(isEmpty(response.data.error))
           {
               $scope.productos = response.data;
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
        wsFactory.getTotalProductos({"filterBy":$scope.filtros}).then(function(response) {
            $scope.total = 0;
            $scope.paginas = 0;
           if(isEmpty(response.data.error))
           {
               $scope.total = response.data.Total;
               $scope.paginas = Math.ceil($scope.total/$scope.itemXpag);
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    
    cargarProductos();
    $rootScope.$watch('reloadProductos', function(reloadComedor) {
         cargarProductos();
    }); 
    $scope.$watch('orderByItem+reverse', function(reloadComedor) {
         cargarProductos();
    }); 
    $rootScope.$watch('reloadCategorias', function(reloadComedor) {
         cargarCategorias();
    }); 
    
    
    $scope.getMostrar = function()
    {
        var act = ($location.path().indexOf('accion/') > -1);
        return act;
    }
    
    
    //CATEGORIAS
    
    var cargarCategorias = function()
    {
        wsFactory.getCategoriasHijos().then(function(response) {
           if(isEmpty(response.data.error)) $scope.categorias = response.data;
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    
    //EN COMUN
    $scope.eliminar = function (valor,id) {
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-confirm.html',
            className: 'ngdialog-theme-plain',
            showClose: false,
            controller: ['$scope', function(scope) {
                scope.Titulo = "Eliminar "+((valor === 1)? "producto":"categoria");
                scope.Descripcion = "Seguro que desea eliminar";
                if(valor === 1){
                    scope.Descripcion = "¿Seguro que desea eliminar este producto?";
                }
                else{
                    scope.Descripcion = "¿Seguro que desea eliminar esta categoría? Esto puede provocar que algunos productos no pertenezcan a alguna categoría";
                }
            }]
        }).then(function (value) {
           if(value === true)
            {
                eliminar(valor,id);
            }
        }, function (reason) {
            
        });        
    };
    var eliminar = function(valor, id){
        if(!isEmpty(id))
        {
            if(valor === 1)
            {
                 wsFactory.eliminarProducto(id).then(function(response) {
                    if(isEmpty(response.data.error))
                    {
                        $rootScope.reloadProductos = !$rootScope.reloadProductos;
                        $scope.redirigir('/productos');
                    }
                }, function(error) {
                    console.log('Un error ocurrió durante la eliminación ' + error);
                });
            }
            else if(valor === 2){
                wsFactory.eliminarCategoria(id).then(function(response) {
                    if(isEmpty(response.data.error))
                    {
                        $rootScope.reloadCategorias = !$rootScope.reloadCategorias;
                        $scope.redirigir('/productos','ruta','categorias');
                    }
                }, function(error) {
                    console.log('Un error ocurrió durante la eliminación ' + error);
                });
            }
        }
    }
}]);