angular.module('CofradesApp').controller('CocinaController',['$rootScope','$scope','webSocket','wsFactory','ngAudio', function($rootScope, $scope, webSocket,wsFactory,ngAudio) {
    $scope.pedidos = [];
    $scope.Resumen = [];
    $scope.origen = 'pagina-cocina';
    $scope.sound = ngAudio.load("assets/sonidos/timbre.mp3");
    
       
    /*Resumir*/
    var existe = function(id){
        var comArr = eval( $scope.Resumen);
        if(comArr)
        {
            for( var i = 0; i < comArr.length; i++ ) {
                var elem = comArr[i];
                if(elem['Producto_id'] === id)
                {
                    return elem;
                }
            }
        }
        return false;
    }
    
    var actualizarCantidad = function(Producto_id, campos, valores){
        var comArr = eval( $scope.Resumen);
        if(comArr)
        {
            for( var i = 0; i < comArr.length; i++ ) {
                var elem = comArr[i];
                if(elem['Producto_id'] === Producto_id)
                {
                    var obj = comArr[i];
                    //obj['campo'] = valores;
                    for( var j = 0; j < campos.length; j++ ) {
                        obj[campos[j]] = valores[j];
                    }
                    break;
                }
            }
        }
    } 
    $scope.productoAgregado = function(id){
        if($scope.Resumen)
        {
            for(var i = 0; i < $scope.Resumen.length; i++)
            {
                var elem = $scope.listaProductos[i];
                if(elem['Producto_id'] === id)
                {
                    return elem;
                }
            }
        }
        return false;
    }
    var Resumir = function(){
        if($scope.pedidos){
            $scope.Resumen = [];
            var pedido = eval( $scope.pedidos);
            for( var i = 0; i < pedido.length; i++ ) {
                var elem = pedido[i];
                if(elem)
                {
                    pdidos = elem.Pedidos;
                    //console.log(elem.Pedidos);
                    for( var j = 0; j < pdidos.length; j++ ) {
                        var elem = pdidos[j];
                        
                            if(existe(elem['Producto_id']) === false)
                            {
                                $scope.Resumen.push({'Producto_id': elem.Producto_id,'Producto': elem.Nombre, 'Cantidad':elem.Cantidad,'EstadoDetalle_id':elem.EstadoDetalle_id});
                            }
                            else{
                                for(var k = 0; k < $scope.Resumen.length; k++)
                                {
                                    var res = $scope.Resumen[k];
                                    if(elem.Producto_id === res.Producto_id)
                                    {
                                        res.Cantidad = parseInt(res.Cantidad ) + parseInt(elem.Cantidad );
                                        break;
                                    }
                                }
                            }
                    }
                }
            } 
        }
    }
    
    
    var ws = webSocket.getWS();
    ws.onMessage(function (msg) {
        if(msg)
        {
            var datos = JSON.parse(JSON.parse(msg.data).message);
            if(datos){
                if(datos.Destino.indexOf($scope.origen) > -1)
                {
                    if(!(datos.Destino.indexOf(datos.Origen) > -1))
                    {
                        actualizar = true;
                    }
                    getPedidos();
                }
            }
        }
    });
    
    var actualizar = false;
    $scope.$watch('pedidos', function(newVal, oldVal){
        Resumir();
        if(actualizar === true)
        {
            if($scope.sound)
            {
                $scope.$evalAsync(
                    function( $scope ) {
                        $scope.sound.play();
                    }
                );                
            }
            actualizar = false;
        }
    }, true);
    
    
    var getPedidos = function()
    {
        wsFactory.getPedidos().then(function(response) {
            if((($scope.pedidos)? ($scope.pedidos.length == 1):false) === true)
            {
                $scope.pedidos =[];
            }
            if(isEmpty(response.data.error))
            {
                $scope.pedidos = response.data;
                
            }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getPedidos();
    
    $scope.cambiarEstado = function(id, estado)
    {
        wsFactory.cambiarEstadoPedido(id,estado).then(function(data) {
            if(data.data.data === true)
            {
                getPedidos();
                $rootScope.msjSocket($scope.origen, ['pagina-pedidoslistos','pagina-cocina','pagina-pedidos'],'cambio-pedido');
            }
        });        
    }        
}]);