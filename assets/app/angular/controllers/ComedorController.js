angular.module('CofradesApp').controller('ComedorController', ['wsFactory','$rootScope','$scope','$location','webSocket','ngAudio',function(wsFactory, $rootScope, $scope, $location,webSocket,ngAudio) {
    $scope.Mesas;
    $scope.origen = 'pagina-comedor';
    $rootScope.reload
    $scope.sound = ngAudio.load("assets/sonidos/timbre.mp3");
    
    var cargarMesas = function(){
        wsFactory.getMesas().then(function(response) {
            if(isEmpty(response.data.error)) $scope.Mesas = response.data;
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    cargarMesas();
    $rootScope.$watch('reloadComedor', function(reloadComedor) {
         cargarMesas();
    });
    $rootScope.$watch('reloadPedidos', function(reloadComedor) {
         getPedidosListos();
    });
    $rootScope.$watch('reloadBebidas', function(reloadComedor) {
         getBebidas();
    });
          
    
    /*Funciones Locales*/
    /*Funciones Scope*/
    $scope.getMostrar = function()
    {
        var act = ($location.path().indexOf('mesa/') > -1 || $location.path().indexOf('nueva/mesa') > -1);
        return act;
    }
    $scope.getTipo = function()
    {
        var val = 1;
        if($location.path().indexOf('mesa/') > -1)
            val = 1;
        else if($location.path().indexOf('nueva/mesa') > -1)
            val =  2;
        return val;
    }
    
    
    var ws = webSocket.getWS();
    ws.onMessage(function (msg) {
        if(msg)
        {
            var datos = JSON.parse(JSON.parse(msg.data).message);
            if(datos){
                if(datos.Mensaje === 'cargar-mesas') cargarMesas();
                if(datos.Destino.indexOf('pagina-pedidoslistos') > -1)
                {
                    if(datos.Origen !== 'pagina-pedidoslistos')
                    {
                        actualizar = true;
                    }
                    if(datos.Mensaje === 'cambio-pedido') getPedidosListos();
                }
                if(datos.Destino.indexOf('pagina-bebidas') > -1)
                {
                    if(datos.Origen !== 'pagina-bebidas')
                    {
                        actualizar = true;
                    }
                    if(datos.Mensaje === 'cambio-bebidas' || datos.Mensaje === 'nuevo-pedido')
                    {
                        getBebidas();
                    }
                }
            }
        }
    });
    var actualizar = false;
    $scope.$watch('ordenes+bebidas', function(newVal, oldVal){
        if(actualizar === true)
        {
            if($scope.sound)
            {
                $scope.$evalAsync(
                    function( $scope ) {
                        $scope.sound.play();
                    }
                );                
            }
            actualizar = false;
        }
    }, true);
    var getPedidosListos = function()
    {
        if((($scope.ordenes)? ($scope.ordenes.length == 1):false) === true)
            {
                $scope.ordenes =[];
            }
        wsFactory.getPedidosListos().then(function(response) {
            if(isEmpty(response.data.error)) $scope.ordenes = response.data;
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getPedidosListos();
    var getBebidas = function()
    {
        if((($scope.bebidas)? ($scope.bebidas.length == 1):false) === true)
        {
            $scope.bebidas =[];
        }
        wsFactory.getBebidas().then(function(response) {
            if(isEmpty(response.data.error)) $scope.bebidas = response.data;
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getBebidas();
    
    $scope.cambiarEstado = function(id, estado, tipo)
    {
        wsFactory.cambiarEstadoPedido(id,estado).then(function(data) {
            
            if(data.data.data === true)
            {
                if(tipo === 1)
                {
                    getPedidosListos();                
                    $rootScope.msjSocket('pagina-pedidoslistos', ['pagina-pedidoslistos','pagina-pedidos'],'cambio-pedido');
                    $rootScope.reloadDetalleMesa = !$rootScope.reloadDetalleMesa;
                    $rootScope.reloadBebidas = !$rootScope.reloadBebidas;                    
                }
                else
                {
                    getBebidas();
                    $rootScope.msjSocket('pagina-bebidas', ['pagina-pedidoslistos','pagina-bebidas','pagina-pedidos'],'cambio-bebidas');
                    $rootScope.reloadPedidos = !$rootScope.reloadPedidos;
                    $rootScope.reloadDetalleMesa = !$rootScope.reloadDetalleMesa;
                }
            }
        });
    }
}]);