angular.module('CofradesApp').controller('PerfilController', ['wsFactory','$rootScope','$scope','$location','$window',"$stateParams",function(wsFactory, $rootScope, $scope, $location,$window,$stateParams) {
    $scope.Usuario_;
    $scope.Usuario;
    $scope.Nombre;
    $scope.Apellidos;
    $scope.Rol_id = 4;    
    $scope.Contrasenia;    
    
    
    var getUsuario = function(){
        wsFactory.getUsuario().then(function(response) {
           if(isEmpty(response.data.error))
           {
               $scope.Usuario_ = response.data;
               $scope.Usuario = $scope.Usuario_.Usuario;
               $scope.Nombre = $scope.Usuario_.Nombre;
               $scope.Apellidos = $scope.Usuario_.Apellidos;
               $scope.Rol_id = $scope.Usuario_.Rol_id;
           }
           else console.log('Un error ocurrió durante la carga ' + error);
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    getUsuario();
    
    
    $scope.guardar = function(){
        
        if(!isEmpty($scope.Nombre) && !isEmpty($scope.Apellidos) && !isEmpty($scope.Usuario))
        {
             var datos = {'user_id':$scope.Usuario_.Usuario_id,'user':$scope.Usuario,'nombre':$scope.Nombre,'apellidos':$scope.Apellidos,'password':null,'newpassword':null,'passwordr':null};
            if($scope.cambiar)
            {
                if(!isEmpty($scope.Contrasenia) && !isEmpty($scope.newContrasenia) && !isEmpty($scope.Contraseniar))
                {
                    datos.password = $scope.Contrasenia;
                    datos.newpassword = $scope.newContrasenia;
                    datos.passwordr = $scope.Contraseniar;
                }
                else $scope.mensaje = {'tipo':'error','contenido':'Todos los campos en éste formulario, deben ser llenados'};
            }
            
             wsFactory.cambiarDatosUsuario(datos).then(function(data) {
                if(isEmpty(data.data.error))
                {
                    $scope.cambiar = false;
                    $scope.Contrasenia = "";
                    $scope.newContrasenia = "";
                    $scope.Contraseniar = "";
                    $scope.mensaje = {'tipo':'info','contenido':'La información se actulizó exitosamente'};
                }
                else{
                    $scope.mensaje = {'tipo':'error','contenido':'Por favor, verifique los datos ingresados'};
                }
            });
        }
        else{
            $scope.mensaje = {'tipo':'error','contenido':'Por favor, verifique los datos ingresados'};
        }
    }
    
    $scope.cerrar = function()
    {
        $rootScope.reloadUsuarios = !$rootScope.reloadUsuarios;
        $rootScope.redirigir('/');
    }    
}]);
