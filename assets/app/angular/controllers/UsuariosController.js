angular.module('CofradesApp').controller('UsuariosController',['$rootScope','$scope','wsFactory','$location','ngDialog', function($rootScope, $scope, wsFactory,$location,ngDialog) {
    $scope.usuarios;
        
    $scope.$watch('reloadUsuarios', function(nuevo, anterior) {
        cargarUsuarios();
    });
     
    var cargarUsuarios = function(){
         wsFactory.getUsuarios().then(function(response) {
           $scope.usuarios = [];
           if(isEmpty(response.data.error))
           {
               $scope.usuarios = response.data;
           }
        }, function(error) {
            console.log('Un error ocurrió durante la carga ' + error);
        });
    }
    
    cargarUsuarios();
    
    $scope.getMostrar = function()
    {
        var act = ($location.path().indexOf('accion/') > -1);
        return act;
    }
    
    //EN COMUN
    $scope.eliminar = function (valor,id) {
        ngDialog.openConfirm({
            template: 'assets/app/plantillas/varias/dialog-confirm.html',
            className: 'ngdialog-theme-plain',
            showClose: false,
            controller: ['$scope', function(scope) {
                scope.Titulo = "Eliminar usuario";
                scope.Descripcion = "Seguro que desea eliminar este usuario?";
            }]
        }).then(function (value) {
           if(value === true)
            {
                eliminar(valor,id);
            }
        }, function (reason) {
            
        });        
    };
    var eliminar = function(valor, id){
        if(!isEmpty(id))
        {
            wsFactory.eliminarUsuario(id).then(function(response) {
                if(isEmpty(response.data.error))
                {
                    $rootScope.reloadUsuarios = !$rootScope.reloadUsuarios;
                    $scope.redirigir('/usuarios');
                }
            }, function(error) {
                console.log('Un error ocurrió durante la eliminación ' + error);
            });
        }
    }
}]);