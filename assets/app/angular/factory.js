CofradesApp.factory('wsFactory', ['$http', function($http) {
    
    var urlBase = 'api/v1/';
    var config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    }
    
    /*Global*/
    var getIp = function () {
        return $http.post('getIp');
    };
    
    var getSecciones = function () {
        return $http.post(urlBase + 'usuario/secciones');
    };
    /*Usuario*/
    var getUsuario = function () {
        return $http.post(urlBase + 'usuario/get');
    };
    var getUsuarioWS = function (id) {
        return $http.post(urlBase + 'usuario/get/'+id);
    };
    var getUsuarios = function () {
        return $http.post(urlBase + 'usuario/all');
    };
    var crearUsuario = function(datos){
        return $http.post("api/v1/usuario/crear",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var eliminarUsuario = function(id){
        return $http.post("api/v1/usuario/eliminar/"+id);
    };
    var getRoles = function () {
        return $http.post(urlBase + 'usuario/roles');
    };
    var cambiarDatosUsuario = function(datos){
        return $http.post("actualizarPerfil",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    /*Mesas*/
    var getMesas = function () {
        return $http.post(urlBase + 'mesas/all');
    };
    var getMesa = function (MesaId) {
        return $http.post(urlBase+'mesas/get/'+MesaId);
    };
    var crearMesa = function(datos){
        return $http.post("api/v1/mesas/crear",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var eliminarMesa = function(id){
        return $http.post("api/v1/mesas/eliminar/"+id);
    };
    var cambiarMesa = function(pedido, mesa){
        return $http.post("api/v1/mesas/cambiar/"+pedido+"/"+mesa);
    };
    /*Productos*/
    var getProducto = function (id) {
        return $http.post(urlBase+'productos/get/'+id);
    };
    var getTotalProductos = function (datos) {
        return $http.post(urlBase+'productos/total',datos, config).error(function (data, status, headers, config) {
            console.log("Error en busqueda");
        });
    };
    var getProductos = function () {
        return $http.post(urlBase+'productos/all');
    };
    var getProductosFull = function (datos) {
        return $http.post(urlBase+'productos/full',datos, config).error(function (data, status, headers, config) {
            console.log("Error en busqueda");
        });
    };
    var crearProducto = function(datos){
        return $http.post("api/v1/productos/crear",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var eliminarProducto = function(id){
        return $http.post("api/v1/productos/eliminar/"+id);
    };
    /*Pedidos*/
    var cambiarEstadoPedido = function(id, estado){
        return $http.post("api/v1/pedidos/cambiarEstado/"+id+"/"+estado);
    };
    var getPedidos = function () {
        return $http.post(urlBase+'pedidos/all');
    };
    var getBebidas = function () {
        return $http.post(urlBase+'pedidos/bebidas');
    };
    var getPedidosListos = function () {
        return $http.post(urlBase+'pedidos/getListos');
    };
    
    var getPedidoMesa = function (Mesa_id) {
        return $http.post(urlBase+'pedidos/getPedidoMesa/'+Mesa_id);
    };
    var getEntregadoMesa = function (Mesa_id) {
        return $http.post(urlBase+'pedidos/getEntregadoMesa/'+Mesa_id);
    };
     
    /*Funciones para inserción, actualización*/
    var crearPedido = function(datos){
        return $http.post("api/v1/pedidos/crear",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var pagoParcial = function(datos){
        return $http.post("api/v1/pedidos/pagoParcial",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var pagoTotal = function(datos){
        return $http.post("api/v1/pedidos/pagoTotal",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    
    /*Categorias*/
    var crearCategoria = function(datos){
        return $http.post("api/v1/categorias/crear",datos, config).error(function (data, status, headers, config) {
            console.log("Error en creación");
        });
    };
    var getCategoria = function (id) {
        return $http.post(urlBase+'categorias/get/'+id);
    };
    var getCategorias = function () {
        return $http.post(urlBase+'categorias/getCategorias');
    };
    var getCategoriasHijos = function () {
        return $http.post(urlBase+'categorias/getHijos');
    };
    var getCategoriasTop = function () {
        return $http.post(urlBase+'categorias/getTop');
    };
    var getArbolCategorias = function () {
        return $http.post(urlBase+'categorias/getArbol');
    };
    var getproductosCategoria = function (id) {
        return $http.post(urlBase+'categorias/getproductosCategoria/'+id);
    };
    var eliminarCategoria = function(id){
        return $http.post("api/v1/categorias/eliminar/"+id);
    };
    
    
    /*Reportes*/
    var getInformeVentas = function (datos) {
        return $http.post(urlBase+'informes/ventas',datos, config).error(function (data, status, headers, config) {
            console.log("Error en busqueda");
        });
    };
    var getInformeVentasFecha = function (datos) {
        return $http.post(urlBase+'informes/ventasFecha/'+datos);
    };
    var getTotalVendido = function () {
        return $http.post(urlBase+'informes/total');
    };
    var getInformeVenta = function (datos) {
        return $http.post(urlBase+'informes/venta/'+datos);
    };
    var getInformeProductos = function (datos) {
        return $http.post(urlBase+'informes/productos',datos, config).error(function (data, status, headers, config) {
            console.log("Error en busqueda");
        });
    };
    var getInformeAfluencias = function (datos) {
        return $http.post(urlBase+'informes/afluencias',datos, config).error(function (data, status, headers, config) {
            console.log("Error en busqueda");
        });
    };
    return {
        //Global
        getIp: getIp,        
        getSecciones: getSecciones,
        //Usuario
        getUsuario: getUsuario,
        getUsuarioWS: getUsuarioWS,
        getUsuarios: getUsuarios,
        crearUsuario: crearUsuario,
        eliminarUsuario:eliminarUsuario,
        getRoles:getRoles,
        cambiarDatosUsuario:cambiarDatosUsuario,
        //Mesas
        getMesas: getMesas,
        getMesa: getMesa,
        crearMesa: crearMesa,
        eliminarMesa:eliminarMesa,
        cambiarMesa:cambiarMesa,
        //Productos
        getProducto: getProducto,
        crearProducto: crearProducto,
        getProductos: getProductos,
        getTotalProductos: getTotalProductos,
        getProductosFull: getProductosFull,
        getproductosCategoria: getproductosCategoria,
        eliminarProducto:eliminarProducto,
        //Pedidos
        cambiarEstadoPedido:cambiarEstadoPedido,
        crearPedido : crearPedido,
        pagoParcial : pagoParcial,
        pagoTotal : pagoTotal,
        getEntregadoMesa : getEntregadoMesa,
        getPedidoMesa : getPedidoMesa,
        getPedidos : getPedidos,
        getBebidas : getBebidas,
        getPedidosListos : getPedidosListos,
        //Categorias
        crearCategoria: crearCategoria,
        getCategoria:getCategoria,
        getCategorias:getCategorias,
        getCategoriasHijos:getCategoriasHijos,
        getCategoriasTop:getCategoriasTop,
        getArbolCategorias:getArbolCategorias,
        getproductosCategoria:getproductosCategoria,
        eliminarCategoria:eliminarCategoria,
        //Informes
        getTotalVendido:getTotalVendido,
        getInformeVentas:getInformeVentas,
        getInformeVentasFecha:getInformeVentasFecha,
        getInformeVenta:getInformeVenta,
        getInformeProductos:getInformeProductos,
        getInformeAfluencias:getInformeAfluencias,
    };
}]);
CofradesApp.factory('Auth', ['$http','$q', function($http,$q) {
var usuario;
return{
    setUsuario : function(user){
        usuario = user;
    },
    estaLogueado : function(){
        return(usuario)? usuario : false;
    }, 
    AuthAsync : function(){
        var deferred = $q.defer(); 
        $http.post('api/v1/usuario/get').success(function(user){
            if (user){
                if(isEmpty(user.error) === true)
                {
                    deferred.resolve(user);
                }
                deferred.resolve(false);
            }
            else { 
                deferred.reject("No se encontró el usuario");
            } 
        }); 
        return deferred.promise; 
    }
}
}]);

CofradesApp.factory('webSocket',['ngSocket','$q','wsFactory', function(ngSocket,$q,wsFactory) {
    var ip = "192.168.1.1";
    var ws = ngSocket('ws://'+ip+':8080');
    wsFactory.getIp().then(function(response) {
        ip = response.data;
        ws = ngSocket('ws://'+ip+':8080');
    }, function(error) {
        console.log('Un error ocurrió durante la carga ' + error);
    });    
    
    ws.onOpen(function () {
        console.log('connection open');
    });
    
    return {
        getWS :function()
        {
            return ws;
        },
        onAsync : function(){
            var deferred = $q.defer(); 
            ws.onMessage(function (msg) {
                deferred.resolve(msg);
            });
            return deferred.promise; 
        },
        estado: function () {
            return ws.readyState;
        },
        enviar: function (message) {
            if (angular.isString(message)) {
                ws.send(message);
            }
            else if (angular.isObject(message)) {
                ws.send(JSON.stringify(message));
            }
        }
    };
    
     // Define a "getter" for getting customer data
    Service.getCustomers = function() {
      var request = {
        type: "get_customers"
      }
      // Storing in a variable for clarity on what sendRequest returns
      var promise = sendRequest(request); 
      return promise;
    }

    return Service;
}]);
