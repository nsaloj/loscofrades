CofradesApp.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('carga-pagina'); // remove page loading indicator
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]);

CofradesApp.directive('tooltip', function($document, $compile) {
    return {
    restrict: 'A',
    scope: true,
    link: function (scope, element, attrs) {

      var tip = $compile('<div ng-class="tipClass">{{ texto }}<div class="tooltip-arrow"></div></div>')(scope),
      tipClassName = 'tooltip',
      tipActiveClassName = 'tooltip-show';

      scope.tipClass = [tipClassName];
      scope.texto = attrs.tooltip;
      
      if(attrs.tooltipLado) {
        scope.tipClass.push('tooltip-' + attrs.tooltipLado);
      }
      else {
       scope.tipClass.push('tooltip-abajo'); 
      }
      $document.find('body').append(tip);
      
      element.bind('mouseover', function (e) {
        tip.addClass(tipActiveClassName);
        
        var pos = e.currentTarget.getBoundingClientRect(),
        offset = tip.offset(),
        tipHeight = tip.outerHeight(),
        tipWidth = tip.outerWidth(),
        elWidth = pos.width || pos.right - pos.left,
        elHeight = pos.height || pos.bottom - pos.top,
        tipOffset = 10;
        
        if(tip.hasClass('tooltip-derecha')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = pos.right + tipOffset;
        }
        else if(tip.hasClass('tooltip-izquierda')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = pos.left - tipWidth - tipOffset;
        }
        else if(tip.hasClass('tooltip-abajo')) {
          offset.top = pos.top + elHeight + tipOffset;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
        }
        else {
          offset.top = pos.top - tipHeight - tipOffset;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
        }

        tip.offset(offset);
      });
      
      element.bind('click', function (e) {
          tip.removeClass(tipActiveClassName);
      });
      element.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
      });

      tip.bind('mouseover', function () {
        tip.addClass(tipActiveClassName);
      });

      tip.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
      });

      
    }
  }
});

CofradesApp.directive('dspaginador', function($document, $compile) {
    return {
      restrict: 'E',
      require: '?ngModel', 
      scope:{
          pagina: '=',
          paginas: '=',
      },
      template: '<div class="dspaginador">'+
                    '<a ng-click="setPagina(1)" class="item-paginador item-paginador-accion">&#60;&#60;</a>'+
                    '<a ng-click="restarPagina(1)" class="item-paginador item-paginador-accion">&#60;</a>'+
                    '<a ng-click="setPagina(paginai)" class="item-paginador" data-ng-class="{\'item-paginador-activo\': (paginai == pagina)}" ng-repeat="paginai in items track by $index">{{paginai}}</a>'+
                    '<a ng-click="aumentarPagina(1)" class="item-paginador item-paginador-accion">&#62;</a>'+
                    '<a ng-click="setPagina(paginas)" class="item-paginador item-paginador-accion">&#62;&#62;</a>'+
                '</div>',
      link: function (scope, element, attrs, ngModel) {
            if(attrs.paginas) scope.paginas = attrs.paginas;
            else scope.paginas = 1;
            if(attrs.limite) scope.limite = attrs.limite;
            else scope.limite = 0;
            scope.items = [];
            
            scope.setPagina = function(pagina){
                if(scope.paginas > 0){
                    scope.pagina = pagina;
                }
            }
            scope.aumentarPagina = function(valor){
                if(scope.paginas > 0){
                    var pagina = scope.pagina+valor;
                    scope.pagina = (pagina > scope.paginas)? scope.paginas:pagina;
                }
            }
            scope.restarPagina = function(valor){
                if(scope.paginas > 0){
                    var pagina = scope.pagina-valor;
                    scope.pagina = (pagina < 1)? 1:pagina;
                }
            }
            scope.getPaginas = function(num) {
                var pags = [];
                for(var i = 1; i <= num; i++){
                    pags.push(i);
                }
                return pags;
            }
            scope.arrayPaginas = scope.getPaginas(scope.paginas);
            
            scope.$watch('paginas', function(nuevo, anterior) {
                if(nuevo > 10)
                {
                    scope.limite = 10;
                }
                else scope.limite = nuevo;
                scope.arrayPaginas = scope.getPaginas(nuevo);
                scope.items = [];
                scope.setItems(1,1);
            });
            scope.$watch('pagina', function(nuevo, anterior) {
                scope.setItems(anterior, nuevo);
            });
            scope.setItems = function(anterior, nuevo)
            {
                if(isEmpty(scope.items) && scope.paginas > 0)
                {
                    for(var i = scope.pagina; i < (scope.pagina+(scope.limite)); i++){
                        if(isEmpty(scope.items)) scope.items = [i];
                        else scope.items.push(i);
                    }
                }
                else if(!isEmpty(scope.items) && scope.paginas > scope.limite){
                    var menor = false;
                    var mayor = false;
                    menor = scope.items[0];
                    mayor = scope.items[scope.limite-1];
                    
                    
                    if(nuevo > anterior && scope.items[(scope.limite/2)] <= scope.pagina )
                    {
                        if(mayor !== scope.arrayPaginas[scope.paginas-1])
                        {
                            scope.items = [];
                            
                            
                            for(var i = 0; i < scope.limite; i++)
                            {
                                 var index = i+(scope.pagina-(scope.limite/2));
                                 if((scope.paginas - scope.pagina) < (scope.limite/2))
                                 {
                                     index = i+(scope.paginas - scope.limite);
                                 }
                                 
                                 scope.items.push(scope.arrayPaginas[index]);
                            }
                        }                        
                    }
                    else if(nuevo < anterior && scope.items[(scope.limite/2)] >= scope.pagina)
                    {
                        if(menor !== scope.arrayPaginas[0])
                        {
                            scope.items = [];
                            for(var i = 0; i < scope.limite; i++)
                            {    
                                 var index = i+(scope.pagina-(scope.limite/2));
                                 if((scope.pagina - (scope.limite/2)) <= 0)
                                 {
                                     index = i;
                                 }
                                 scope.items.push(scope.arrayPaginas[index]);
                            }
                        }                        
                    }
                }                
            }
      }
    }
});

CofradesApp.directive('dssteper', function($document, $compile) {
    return {
      restrict: 'E',
      require: '?ngModel', 
      scope:{
          min: '=',
          max: '=',
          model: '=ngModel'
      },
      template: '<div class="dssteper-class">'+
        '<button ng-click="menos()" class="boton-class dssteper-btn dssteper-btn-menos"><i class="{{iconMenos}}"></i></button>'+
        '<input type="text" class="dssteper-valor" value="{{ value }}"/>'+
        //'<label class="dssteper-valor"> {{ value }}</label>'+
        '<button ng-click="mas()" class="boton-class dssteper-btn dssteper-btn-mas"><i class="{{iconMas}}"></i></button>'+
        '</div>',
      link: function (scope, element, attrs, ngModel) {
            
            if(attrs.iconMenos) { scope.iconMenos = attrs.iconMenos }
            if(attrs.iconMas) { scope.iconMas = attrs.iconMas }
                      //alert()
           ngModel.$render = function() {
                element.find('.dssteper-valor').val(ngModel.$viewValue);
            };
           element.find('.dssteper-valor').on('blur',function(){
               ngModel.$setViewValue(ngModel.$viewValue);
               ngModel.$render();
           });
           scope.menos = function(){
               updateModel(-1);
           }
           scope.mas = function(){
               updateModel(+1);
           }
           
           function validar() {
                // check if min/max defined to check validity
                var isOverMin = (angular.isDefined(scope.min) && ngModel.$viewValue < parseInt(scope.min, 10)),
                    isOverMax = (angular.isDefined(scope.max) && ngModel.$viewValue > parseInt(scope.max, 10)),
                    valid = !(isOverMin || isOverMax);
                // set our model validity
                // the outOfBounds is an arbitrary key for the error.
                // will be used to generate the CSS class names for the errors
                ngModel.$setValidity('dsstep', valid);
                
            } 
          
           function updateModel(offset) {
                ngModel.$setViewValue(ngModel.$viewValue + offset);
                ngModel.$render();
                validar();
            }
          
            validar();
            scope.$watch('min+max', function() {
                validar();
            });
            scope.$watch("model", function() {
                validar();
            });
      }
    }
});

CofradesApp.directive('dsautocomplete', function($document, $compile) {
    return {
      restrict: 'AE',
      transclude: true,
      scope:{
          modelo: '=',
          array:'='
      },
      templateUrl: 'assets/app/angular/componentes/dsautocomplete.html',
      link: function (scope, element, attrs, ctrl) {
            if(attrs.placeholder) {scope.placeholder = attrs.placeholder}
            if(attrs.campo) {scope.campo = attrs.campo}
            scope.elementos = [];
            scope.$watch('array', function(array) {
                scope.elementos = array;
            });            
          
            scope.resultados = [];
            scope.selected = -1;
            scope.buscar = function() {  
                
                scope.resultados = [];
                var resultados_tam = 0;
                if(scope.elementos)
                {
                    for(var i = 0; i < scope.elementos.length; i++)
                    {
                        var resultado_lwc = angular.lowercase(scope.elementos[i][scope.campo]);
                        if(scope.modelo)
                        {
                            var busqueda_lwc = angular.lowercase(scope.modelo[scope.campo]);
                            if(resultado_lwc.indexOf(busqueda_lwc) != -1)
                            {
                                scope.resultados.push($.extend( {}, scope.elementos[i]));
                                resultados_tam += 1;
                                if(resultados_tam == 6)
                                {
                                    break;
                                }
                            }
                        }
                        else break;
                    }
                    $('.dropdownac-content').css({'display': 'block'});
                }
            }
            
            
             scope.existe = function() {  
                if(scope.elementos)
                {
                    for(var i = 0; i < scope.elementos.length; i++)
                    {
                        var resultado_lwc = angular.lowercase(scope.elementos[i][scope.campo]);
                        if(scope.modelo)
                        {
                            var busqueda_lwc = angular.lowercase(scope.modelo[scope.campo]);
                            if(resultado_lwc === busqueda_lwc)
                            {
                                return $.extend( {}, scope.elementos[i]);
                            }
                        }
                    }
                }
                return false;
            }

            scope.$watch('selected',function(val){
                if(val !== -1) {
                    scope.modelo = $.extend( {}, scope.resultados[scope.selected]);
                }
                else{
                    scope.resultados = [];
                    $('.dropdownac-content').css({'display': 'none'});
                }
            });
            scope.mouseOver = function(index){
                scope.selected = index;
            }            
            
            scope.checkKeyDown = function(event){
                if(event.keyCode === 40){//down key, increment selected
                    event.preventDefault();
                    if(scope.selected+1 < scope.resultados.length){
                        scope.selected++;
                    }else{
                        scope.selected = 0;
                    }
                }else if(event.keyCode === 38){ //up key, decrement selected
                    event.preventDefault();
                    if(scope.selected-1 >= 0){
                        scope.selected--;
                    }else{
                        scope.selected = scope.resultados.length-1;
                    }
                }else if(event.keyCode === 13){ //enter key, empty resultados array
                    event.preventDefault();
                    var valor = scope.existe();
                    if(valor !== false)
                    {
                        scope.modelo = $.extend( {}, valor );
                    }
                    else
                    {
                        scope.resultados = [];
                        scope.selected = -1;
                    }
                    $('.dropdownac-content').css({'display': 'none'});
                }
                else if(event.keyCode === 9){ //ESC key, empty resultados array
                    scope.resultados = [];
                    scope.selected = -1;
                }
                else if(event.keyCode === 27){ //ESC key, empty resultados array
                    event.preventDefault();
                    scope.resultados = [];
                    scope.selected = -1;
                }else{
                    //alert('kdb');
                    scope.buscar();	
                    if(scope.selected === -1) {
                        scope.resultados = [];
                        $('.dropdownac-content').css({'display': 'none'});
                    }
                }
            };
            
            
            document.addEventListener("click", function(event){
                if(event.target !== element) {
                    scope.resultados = [];
                    scope.selected = -1;
                    $('.dropdownac-content').css({'display': 'none'});
                    var valor = scope.existe();
                    if(valor === false)
                    {
                        scope.modelo = [];
                    }
                }
            });

            scope.checkKeyUp = function(event){ 
                if(event.keyCode !== 8 || event.keyCode !== 46){//delete or backspace
                    if(scope.autocomplete === ""){
                        scope.resultados = [];
                        scope.selected = -1;
                    }
                }
            };
            
            scope.AssignValueAndHide = function(index){
                scope.modelo = $.extend( {}, scope.resultados[index]);
                 scope.resultados=[];
                 scope.selected = -1;
                 $('.dropdownac-content').css({'display': 'none'});
            };
      },
    }
});
