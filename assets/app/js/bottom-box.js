function adaptarBotomBox() {
    
    var bbox = $('.bottom-box');

    var viewport_height = getViewPort().height;
    var height = viewport_height*65/100;

    bbox.css({
    'min-height' : height+'px',
    'max-height' : height+'px',
    'height' : height+'px'
    });
}
$(function(){
   adaptarBotomBox();
    $( window ).resize(function() {
        adaptarBotomBox();
    });    
    
    $( '.sidenav' ).on( 'visibility', function() {
		var $element = $( this );
		var timer = setInterval( function() {
			if( $element.is( ':hidden' ) ) {
				adaptarBotomBox();
			} else {
				adaptarBotomBox();
			}
		}, 300 );
	}).trigger( 'visibility' );
});
    