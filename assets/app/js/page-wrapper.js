
function cambiar(top_component) {
    
    var content = $('.page-wrapper');

    var topbar_height =  parseInt($(top_component).outerHeight());
    var viewport_height = getViewPort().height;
    var height = viewport_height;
    
    content.css({
    'padding-top': topbar_height+'px',
    'min-height' : height+'px',
    'height' : height+'px'                  
    });
}
$(function(){
   cambiar('.toolbar');
    $( window ).resize(function() {
        cambiar('.toolbar');
    });
    
});
    
    